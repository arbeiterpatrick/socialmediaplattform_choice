
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   App.js                  *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';

import { TabNavigator, StackNavigator } from "react-navigation";

import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';
import { setContext } from 'apollo-link-context';

import { signIn, signOut, getToken } from './components/util';

import Login from "./components/Login";
import Register from "./components/Register";
import HomeWithData from './components/Home';
import ProfileWithData from './components/Profile';
import SearchWithData from './components/Search';
import SettingsWithData from './components/Settings';
import Logout from './components/Logout';
// import AddPost from './components/AddPost';


// Components which are used in react-navigation, have to be imported

// HomeStack
const HomeStack = StackNavigator({
    Home: {
        screen: HomeWithData,
        navigationOptions: ({navigation}) => ({
            title: 'Home',
            header: null,
        }),
    },
    /*
     AddPost: {
        screen: AddPost,
        navigationOptions: ({navigation}) => ({
            title: 'create a post',
        }),
    },
    */
});

// SettingsStack
const SettingsStack = StackNavigator({
    Settings: {
        screen: SettingsWithData,
        navigationOptions: ({navigation}) => ({
            title: 'Settings',
        }),
    },
    Logout: {
        screen: Logout,
        navigationOptions: ({navigation}) => ({
            title: 'Logout',
        }),
    }
});

// Tabs
const Tabs = TabNavigator({
  Profile: {
      screen: ProfileWithData,
      navigationOptions: ({navigation}) => ({
          header: null,
      }),
  },
  Home: {
      screen: HomeStack,
      navigationOptions: ({navigation}) => ({
          header: null,
      }),
  },
  Search: {
      screen: SearchWithData,
      navigationOptions: ({navigation}) => ({
          header: null,
      }),
  },
    Settings: {
        screen: SettingsStack,
        navigationOptions: ({navigation}) => ({
            header: null,
        }),
    },
}, {
  tabBarPosition: 'bottom',
  animationEnabled: true,
   tabBarOptions: {
     labelStyle: {
       fontSize: 12,
       color: "black",
     },
     style: {
       backgroundColor: 'white',
     },
  },
});

// AuthStack: if loggedIn state is false, you will be forwarded to this stack
const AuthStack = StackNavigator({
    Register: {
        screen: Register,
        navigationOptions: ({navigation}) => ({
            title: 'Register',
        }),
    },
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
          title: 'Login',
        }),
    },
});

// LoggedInStack: if loggedIn state is true, you will be forwarded to this stack
const LoggedInStack = StackNavigator({
  Tabs: {
    screen: Tabs,
  },
});

// httpLink contains link to the Back-End
const httpLink = new HttpLink(
    { uri: 'http://dipl17.ddns.net/v1/graphql' }
);

// authLink contains header information and the authentication token
const authLink = setContext(async (req, { headers }) => {
   const token = await getToken();
   return {
       ...headers,
       headers: {
           authorization: token ? `Bearer ${token}` : "",
       },
   }
});

// link which is used to create the ApolloClient
// consists of auth- and httpLink
const link =  authLink.concat(httpLink);

// ApolloClient is used to connect to Back-End
const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
});

// App Component
export default class App extends Component<{}> {

    // get props from parent component and initialize states
    // loggedIn state is set to false per default
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: false,
        };
    }

    // when component mounts this function will be called
    // if token exists loggedIn state is set to true
    async componentWillMount() {
        const token = await getToken();
        if (token) {
            this.setState({loggedIn: true});
        }
    }

    // when user logs in, function is called
    // if loggedIn is true, token will be passed to signIn
    // if loggedIn is false, active token will be deleted
    handleChangeLoginState = (loggedIn = false, token) => {
        this.setState({loggedIn});
        if (loggedIn) {
            signIn(token);
        } else {
            signOut();
        }
    };

    render() {
        return (
            <!-- ApolloClient is passed to ApolloPorvider !-->
            <ApolloProvider client={client}>
                {
                    /* if loggedIn is true, you will be forwarded to loggedInStack
                       if loggedIn is false, you will be forwarded to authStack */
                    this.state.loggedIn ?
                        <LoggedInStack screenProps={{changeLoginState: this.handleChangeLoginState}}/> :
                        <AuthStack screenProps={{changeLoginState: this.handleChangeLoginState}}/>
                }
            </ApolloProvider>
        )
    }
}
