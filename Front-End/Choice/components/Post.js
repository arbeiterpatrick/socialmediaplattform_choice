
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Post.js                 *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';

export default class Post extends Component {
  render() {
    return (
        <View style = {styles.postContainer}>
            <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{padding:5}}>

                    <!-- profile photo !-->
                    <Avatar
                        small
                        rounded
                        title={this.props.firstname.substr(0,1) + this.props.surname.substr(0,1)}
                        activeOpacity={0.7}
                    />
                </View>

                <!-- First name and surname as headline of the post !-->
                <Text style={styles.title}>
                    {this.props.firstname} {this.props.surname}
                </Text>
            </View>

            <!-- message of the post !-->
            <Text style={styles.postText}>
                {this.props.message}
            </Text>
        </View>
    );
  }
}

// StyleSheet
const styles = StyleSheet.create({
  postText: {
    fontSize: 11,
    padding: 10,
  },
  title: {
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    fontWeight: '300',
    fontSize: 16,
  },
  postContainer: {
    backgroundColor: '#EEEEEE',
  },
})

