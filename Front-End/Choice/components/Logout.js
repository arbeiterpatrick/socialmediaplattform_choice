
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Logout.js               *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { View,  StyleSheet } from 'react-native';
import { Button } from 'react-native-elements'

export default class Logout extends Component {

    // when the user clicks on the logout button this function will be called
    // the login state will be changed to false
    handlelogout = () => {
        return this.props.screenProps.changeLoginState(false);
    };
    render () {
        return (
            <View>
                <View style={styles.container}>

                    <!-- Logout button !-->
                    <Button style={styles.logOutButton} onPress={this.handlelogout} title='Log Out' />
                </View>
            </View>
        );
    }
}

// StyleSheet
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    headline: {
        textAlign:'center',
        fontSize:30,
    },
});
