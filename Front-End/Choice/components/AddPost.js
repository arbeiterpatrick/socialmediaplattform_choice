
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   AddPost.js              *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */

/**
 * This component shows up an error, so the feature, that a user is able to
 * create a post is disabled.
 */

// IMPORTS

import React, { Component } from 'react';
import {graphql} from "react-apollo/index";
import gql from "graphql-tag";
import { Container, Button, Content, Form, Item, Input, Text } from 'native-base';

class AddPost extends Component {

    // get props from parent Component
    // initialize states
    constructor(props) {
        super(props);
        this.state = {
            input: '',
            inputError: false,
        };
    }

    // set value to specific state
    // field is the name of the Input form (name of input = name of state)
    handleInputChange = (field, value) => {
        const newState = {
            ...this.state,
            [field]: value,
        };
        this.setState(newState);
    };

    // When User clicks on Submit Button
    handleSubmit = () => {

        //get state
        const { input } = this.state;

        if (input.length === 0) {
            return this.setState({ inputError: true });
        }
        this.setState({ inputError: false });

        this.props
            .post(input) // execute mutation
            .catch(e => {
                if (/input/i.test(e.message)) {
                    this.setState({ inputError: true });
                    console.log("inputError");
                }
            });
    };

    render () {

        // get state
        const { inputError } = this.state;

        return (
            <Container>
                <Content>
                    <Form>
                        <Item error={inputError}>
                            <Input
                                placeholder="Post here ..."
                                onChangeText={value => this.handleInputChange('input', value)}
                                autoCapitalize="none"
                                autoCorrect={false}
                            />
                        </Item>
                    </Form>

                    <!-- submit button !-->
                    <Button full onPress={this.handleSubmit}>
                        <Text>Post!</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

// this mutation is to send the new post to the back-end
export default graphql(
    gql`
        mutation Post ($input: String!) {
           addPost( input: {
		    message: $input 
		    fileid: ""
		    viewmode: 0
	    }) {
		    message
	    }}
  `,
    {
        props: ({ mutate }) => ({
            post: (input) => mutate({ variables: { input } }),
        }),
    },
)(AddPost);


