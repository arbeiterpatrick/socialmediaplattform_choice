
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Login.js                *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { Container, Button, Content, Form, Item, Input, Text } from 'native-base';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

class Login extends Component {

    // get props from parent Component
    // initialize states
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            emailError: false,
            password: '',
            passwordError: false,
        };
    }

    // set value to specific state
    // field is the name of the Input form (name of input = name of state)
    handleInputChange = (field, value) => {
        const newState = {
            ...this.state,
            [field]: value,
        };
        this.setState(newState);
    };

    // When User clicks on Submit Button
    handleSubmit = () => {

        // get values form states
        const { email, password } = this.state;

        if (email.length === 0) {
            return this.setState({ emailError: true });
        }
        this.setState({ emailError: false });

        if (password.length === 0) {
            return this.setState({ passwordError: true });
        }
        this.setState({ passwordError: false });

        this.props
            .login(email, password) // execute mutation
            .then(({ data }) => {
                return this.props.screenProps.changeLoginState(true, data.loginUser.token); // change login state to true and set user token
            })
            .catch(e => {
                // If the error message contains email or password we'll assume that's the error.
                if (/email/i.test(e.message)) {
                    this.setState({ emailError: true });
                    console.log("emailError");
                }
                if (/password/i.test(e.message)) {
                    this.setState({ passwordError: true });
                    console.log("passwordError");
                }
            });
    };

    render() {
        // get states
        const { emailError, passwordError } = this.state;

        return (
            <Container>
                <Content>
                    <Form>
                        <Item error={emailError}>
                            <Input
                                placeholder="Email"
                                onChangeText={value => this.handleInputChange('email', value)}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />
                        </Item>
                        <Item error={passwordError}>
                            <Input
                                placeholder="Password"
                                onChangeText={value => this.handleInputChange('password', value)}
                                autoCapitalize="none"
                                autoCorrect={false}
                                secureTextEntry
                            />
                        </Item>
                    </Form>

                    <!-- submit button !-->
                    <Button full onPress={this.handleSubmit}>
                        <Text>Sign In</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

// mutation to send the login values to the back-end
export default graphql(
    gql`
    mutation LogIn($email: String!, $password: String!) {
	loginUser(input: {
		email: $email
		password: $password
	}) {
		email
		token
	}
}
  `,
    {
        props: ({ mutate }) => ({
            login: (email, password) => mutate({ variables: { email, password } }),
        }),
    },
)(Login);