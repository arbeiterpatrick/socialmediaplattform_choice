
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Register.js             *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React from 'react';
import { Container, Button, Content, Form, Item, Input, Text } from 'native-base';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

class Register extends React.Component {

    // get props from parent Component
    // initialize states
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            emailError: false,
            password: '',
            passwordError: false,
            confirmPassword: '',
            confirmPasswordError: false,
            firstname: '',
            firstnameError: false,
            surname: '',
            surnameError: false,
        };
    }

    // set value to specific state
    // field is the name of the Input form (name of input = name of state)
    handleInputChange = (field, value) => {
        const newState = {
            ...this.state,
            [field]: value,
        };
        this.setState(newState);
    };

    // When User clicks on Submit Button
    handleSubmit = () => {

        // get values form states
        const {email, password, confirmPassword, firstname, surname} = this.state;

        if (email.length === 0) {
            return this.setState({emailError: true});
        }
        this.setState({emailError: false});

        if (surname.length === 0) {
            return this.setState({surnameError: true});
        }
        this.setState({surnameError: false});

        if (firstname.length === 0) {
            return this.setState({firstnameError: true});
        }
        this.setState({firstnameError: false});

        if (password.length === 0) {
            return this.setState({passwordError: true});
        }
        this.setState({passwordError: false});

        if (confirmPassword.length === 0) {
            return this.setState({confirmPasswordError: true});
        }
        this.setState({confirmPasswordError: false});

        if (password !== confirmPassword) {
            return this.setState({passwordError: true, confirmPasswordError: true});
        }
        this.setState({passwordError: false, confirmPasswordError: false});


        this.props
            .signup(email, password, firstname, surname) // execute mutation
            .then(({data}) => { // get data from mutation
                return this.props.screenProps.changeLoginState(false); // chang the login state to false
            })
            .then(alert("Register successful!"))
            .catch(e => {
                if (/email/i.test(e.message)) {
                    this.setState({ emailError: true });
                }
                if (/password/i.test(e.message)) {
                    this.setState({ passwordError: true });
                }
                if (/firstname/i.test(e.message)) {
                    this.setState({ firstnameError: true });
                }
                if (/surname/i.test(e.message)) {
                    this.setState({ surnameError: true });
                }
            });
    };

    render() {
        // get navigation function
        const { navigation } = this.props;

        // get states
        const { emailError, passwordError, confirmPasswordError, firstnameError, surnameError } = this.state;

        return (
            <Container>
                <Content>
                    <Form>
                        <Item error={emailError}>
                            <Input
                                placeholder="Email"
                                onChangeText={value => this.handleInputChange('email', value)}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />
                    </Item>
                        <Item error={firstnameError}>
                            <Input
                                placeholder="First Name"
                                onChangeText={value => this.handleInputChange('firstname', value)}
                                autoCapitalize="none"
                                autoCorrect={false}
                            />
                        </Item>
                        <Item error={surnameError}>
                            <Input
                                placeholder="Surname"
                                onChangeText={value => this.handleInputChange('surname', value)}
                                autoCapitalize="none"
                                autoCorrect={false}
                            />
                        </Item>
                        <Item error={passwordError}>
                            <Input
                                placeholder="Password"
                                onChangeText={value => this.handleInputChange('password', value)}
                                autoCapitalize="none"
                                autoCorrect={false}
                                secureTextEntry
                            />
                        </Item>
                        <Item last error={confirmPasswordError}>
                            <Input
                                placeholder="Confirm Password"
                                onChangeText={value => this.handleInputChange('confirmPassword', value)}
                                autoCapitalize="none"
                                autoCorrect={false}
                                secureTextEntry
                            />
                        </Item>
                    </Form>
                    <!-- submit button !-->
                    <Button full onPress={this.handleSubmit}>
                        <Text>Sign Up</Text>
                    </Button>
                    <!-- Navigate to Login !-->
                    <Button full transparent onPress={() => navigation.navigate('Login')}>
                        <Text>Sign In</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

// mutation to send the new user to the back-end
export default graphql(
    gql`
        mutation SignUp (
		    $email: String!, $password: String!, $firstname: String!, $surname: String!
		){
	    addUser(input: {
		    firstname: $firstname
		    surname: $surname
		    email: $email
		    aboutme: ""
		    residence: ""
	    	birthday: "11-08-1999"
		    emplyment: ""
		    fontface: ""
		    password: $password
	    	backgroundcolor: ""
		    fontsize: 18
	    }){
	    	persid
	    }}
    `,{
        props: ({ mutate }) => ({
            signup: (email, password, firstname, surname) => mutate({variables: {email, password, firstname, surname}})
        }),
    })(Register);
