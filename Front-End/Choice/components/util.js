
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   util.js                 *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import { AsyncStorage } from 'react-native';

// define variables
const AUTH_TOKEN = 'AUTH_TOKEN';
let token;

// get token from AsyncStorage
export const getToken = async () => {
    if (token) {
        return Promise.resolve(token);
    }
    token = await AsyncStorage.getItem(AUTH_TOKEN);
    return token;
};

// save the new Token in AsyncStorage
export const signIn = (newToken) => {
    token = newToken;
    return AsyncStorage.setItem(AUTH_TOKEN, newToken);
};

// remove the token from AsyncStorage
export const signOut = () => {
    token = undefined;
    return AsyncStorage.removeItem(AUTH_TOKEN);
};
