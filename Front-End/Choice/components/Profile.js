
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Profile.js              *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { Avatar, Divider } from 'react-native-elements';
import Post from './Post';
import AboutMe from './AboutMe';

// query to get information about the current user
const query = gql`
query {
	users(filter: {
		current: true
	}) {
		persid 
		firstname
		surname
		birthday
		email
		residence
		aboutme
		employment
		posts {
			message
			firstname
			surname
		}
	}
}
`;

class Profile extends Component {
    render() {

        // get data from query
        const { data } = this.props;

        return (
            <View>

                <!-- FlatList is used for showing user details of the current user !-->
                <!-- data needs an array !-->
                <!-- renderItem needs a function for rendering an item !-->
                <FlatList
                    data={data.users}
                    renderItem={this._renderProfileDetails}
                />
            </View>
        );
    }

    // render posts
    // item consists of first name, surname and message
    _renderPosts = ({item}) => (
        <View style={styles.container}>

            <!-- pass variables to the Post component !-->
            <Post
                firstname={item.firstname}
                surname={item.surname}
                message={item.message}
            />
        </View>
    );

    // render profile details
    // item consists of persid, firstname, surname, birthday, email, residence, aboutme, employment and posts
    _renderProfileDetails = ({item}) => (
        <View>
            <View style={{padding: 20, alignItems: 'center', }}>

                <!-- profile photo !-->
                <Avatar
                    xlarge rounded
                    title={item.firstname.substr(0,1) + item.surname.substr(0,1)}
                    onPress={() => console.log("Works!")}
                    activeOpacity={0.7}
                />
            </View>
            <View style={styles.container}>

                <!-- first name and surname as Headline !-->
                <Text style={styles.headline}>{item.firstname} {item.surname}</Text>

                <View style={{padding:10, paddingLeft: 30, paddingRight:30}}>
                    <Divider style={{ backgroundColor: 'black' }} />

                    <!-- information about the user !-->
                    <View style={styles.container}>

                        <!-- check if parameter exists
                             if the parameter exits, it will be showed !-->
                        { item.email ? <Text>email: {item.email} </Text> : <Text></Text> }
                        { item.birthday ? <Text>birthday: {item.birthday} </Text> : <Text></Text> }
                        { item.residence ? <Text>residence: {item.residence} </Text> : <Text></Text> }
                        { item.employment ? <Text>employment: {item.employment} </Text> : <Text></Text> }
                    </View>
                    <Divider style={{ backgroundColor: 'black' }} />

                    <!-- about me !-->
                    <!-- check if parameter exists
                             if the parameter exits, it will be showed !-->
                    { item.aboutme ? <AboutMe aboutme={item.aboutme}/>: <Text></Text>}
                </View>

                <!-- FlatList is used for showing the posts of the current user !-->
                <!-- data needs an array !-->
                <!-- renderItem needs a function for rendering an item !-->
                <FlatList
                    data={item.posts}
                    renderItem={this._renderPosts}
                />
            </View>
        </View>
    );
}

// StyleSheet
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    headline: {
        textAlign:'center',
        fontSize:30,
    },
})

export default ProfileWithData = graphql(query)(Profile);
