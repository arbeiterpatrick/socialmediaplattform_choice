
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Home.js                 *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import Post from './Post';
import { Button } from 'react-native-elements';

// this query is to get all posts
const postsQuery = gql`
  query {
	posts {
		message
		firstname
		surname
	}
}
`;

class Home extends Component {

    // render posts
    // item consists of first name, surname and message
    _renderPosts = ({item}) => (
        <View style={styles.postContainer}>
            <Post
                firstname={item.firstname}
                surname={item.surname}
                message={item.message}
            />
        </View>
    );

    render() {

        // get navigation function
        const { navigate } = this.props.navigation;

        // get data from query
        const { data } = this.props;

        return (
            <View>
                <View style={styles.container}>

                    <!-- no functionality !-->
                    <Button
                        title="create a post"
                        onPress={() =>
                            navigate('AddPost')
                        }
                    />
                </View>

                <!-- FlatList is used for showing all posts !-->
                <!-- data needs an array !-->
                <!-- renderItem needs a function for rendering an item !-->
                <FlatList
                    data={data.posts}
                    renderItem={this._renderPosts}
                />
            </View>
        );
    }
}

// StyleSheet
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
  postContainer: {
    justifyContent:'space-between',
    padding: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

 export default HomeWithData = graphql(postsQuery)(Home);
