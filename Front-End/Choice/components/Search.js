
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Search.js               *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import {Text, View, FlatList, StyleSheet} from 'react-native';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { SearchBar } from 'react-native-elements';

// this query is used to get all registered users
const query = gql`
    query{
	    users {
            firstname
            surname
        }
    }
`;

class Search extends Component {

    // initialize states
    constructor(props) {
        super(props);
        this.state = {text: ''};
    }

    // render Item
    // item consists of first name and surname
    _renderItem = ({item}) => (
        <View style={styles.container}>
            <Text> {item.firstname} {item.surname} </Text>
        </View>
    );

    render() {

        // get data from query
        const { data } = this.props;

        return (
            <View>
                <!-- SearchBar has no functionality !-->
                <SearchBar
                    lightTheme
                    icon={{ type: 'font-awesome', name: 'search' }}
                    placeholder='Search User'
                />

                <!-- FlatList is used for showing all registered users !-->
                <!-- data needs an array, for example an array of registered users!-->
                <!-- renderItem needs a function for rendering an item !-->
                <FlatList
                    data={data.users}
                    renderItem={this._renderItem}
                />
            </View>
        );
    }
}

// StyleSheet
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
});

export default SearchWithData = graphql(query)(Search);
