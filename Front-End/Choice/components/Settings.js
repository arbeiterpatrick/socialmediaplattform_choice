
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   Settings.js             *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { View,  StyleSheet } from 'react-native';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { List, ListItem, Button } from 'react-native-elements'

// this query is used to get all information of the current user
const query = gql`
query {
	users(filter: {
		current: true
	}) {
		persid 
		firstname
		surname
		birthday
		email
		residence
		aboutme
		employment
		posts {
			message
			firstname
			surname
		}
	}
}
`;

class Settings extends Component {
    render() {

        // get data from query
        const { data } = this.props;

        // get navigation function
        const { navigate } = this.props.navigation;

        return (
            <View>
                <List>
                    <!-- no functionality !-->
                    <ListItem
                        title="Edit Profile"
                    />

                    <!-- if you click on this ListItem,  you will be forwarded to Logout component !-->
                    <ListItem
                        title="Log out"
                        onPress={() =>
                            navigate('Logout')
                        }
                    />
                </List>
            </View>
        );
    }
}

export default SettingsWithData = graphql(query)(Settings);
