
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   AboutMe.js              *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// IMPORTS

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Divider } from 'react-native-elements';

export default class AboutMe extends Component {
    render() {
        return (
            <View>
                <View style={{padding:10}}>
                    <Text>{this.props.aboutme}</Text>
                </View>
                <Divider style={{ backgroundColor: 'black' }} />
            </View>
        );
    }
}

