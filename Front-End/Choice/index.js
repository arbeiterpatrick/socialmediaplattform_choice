
/* ------------------------------- *
 * Social Media Plattform "Choice" *
 *          Diplomarbeit           *
 * ------------------------------- *
 * File:   index.js                *
 * Author: Patrick Arbeiter        *
 * ------------------------------- */


// In index.js the App Component is called

// IMPORTS

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import App from './App';

class Index extends Component<{}> {
  render() {
    return (
        <App />
    );
  }
}

AppRegistry.registerComponent('Choice', () => Index);
