package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.datastax.driver.core.Row;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class E10_Modie extends E9_Theme {
    //This class has no special functionality, it just supports the frontend, where the real magic is happening

    public Map<String, Object> setModie(String token, String name) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("modie_id", getRandomUUID());
        map.put("modie_name", name);

        //update the modie
        if (!casDB.Update("user", map, "persid=" + getPersIDFromToken(token))) {
            addUserError(getPersIDFromToken(token), "Could not set the modie", null);
            return null;
        }

        l().info("Setting the modie " + name + " was successful");

        return getModie(token).get(0);

    }

    public List<Map<String, Object>> getModie(String token) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        //fetch the post
        Row r = casDB.Select("post", new String[]{"modie_name", "modie_id"}, "persid=" + getPersIDFromToken(token), 0, null, null).one();

        List<Map<String, Object>> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();

        map.put("name", r.getString(0));
        map.put("id", r.getObject(1).toString());

        list.add(map);

        l().info("The modie was successful fetched from the database.");

        return list;
    }


}
