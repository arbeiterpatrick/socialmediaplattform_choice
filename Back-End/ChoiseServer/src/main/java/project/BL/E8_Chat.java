package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.datastax.driver.core.Row;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class E8_Chat extends E7_Like {

    public List<Map<String, Object>> getChat(String token, String id, String persid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (id != null && !casDB.isUUID(id)) {
            addUserError(getPersIDFromToken(token), "The id is not valid", id);
            return null;
        }

        //prepare the query
        String[] cols = new String[]{"persid2_user", "chatid", "messagepreview", "received_timestamp", "unreadmessages"};
        id = id != null ? " AND chatid=" + id : (persid != null ? " AND persid2_user=" + persid : "");

        //run the query
        List<Row> entry = casDB.Select("chat", cols, "persid1_user=" + getPersIDFromToken(token) + id, 0, null, null).all();

        if (entry.size() > 0) {
            l().info("Fetching the likes were successful");
        } else {
            l().info("The like was not found");
            return null;
        }

        List<Map<String, Object>> list = new LinkedList<>();

        //rename the keys
        for (Row r : entry) {
            Map<String, Object> map = new HashMap<>();
            map.put("persid", r.getObject(0).toString());

            for (int i = 1; i < cols.length - 1; i++) {
                map.put(cols[i], r.getObject(i) == null ? null : r.getObject(i).toString());
            }
            map.put("unreadmessages", r.getInt("unreadmessages"));

            list.add(map);

        }

        return list;
    }

    public Map<String, Object> deleteChat(String token, String id) {
        if (!casDB.isUUID(id)) {
            addUserError(null, "The id is not valid", id);
            return null;
        }

        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);

        //delete the chat
        if (!casDB.Delete("chat", "chatid=" + id)) {
            addUserError(getPersIDFromToken(token), "Neo: Could not delete the chat " + id, id);
            return null;
        }

        l().info("The chat was successful removed");

        return map;
    }

    private boolean chatExists(String persid1, String persid2) {
        return casDB.isUUID(persid1) && casDB.isUUID(persid2) && casDB.Select("chat", new String[]{"chatid"}, "persid1_user=" + persid1 + " AND persid2_user=" + persid2, 0, null, null).all().size() > 0;
    }

    private Map addChat(String token, String persid, String message) {

        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (!userExists(persid)) {
            addUserError(null, "The user does not exist.", persid);
            return null;
        }

        //prepare a chat
        Map<String, Object> map = new HashMap<>();
        map.put("chatid", casDB.getRandomUUID());
        map.put("persid1_user", getPersIDFromToken(token));
        map.put("persid2_user", persid);
        map.put("unreadmessages", 0);
        map.put("messagepreview", (message.length() > 30 ? message.substring(0, 30) : message) + "...");

        //insert the chat
        if (!casDB.Insert("chat", map)) {
            addUserError(getPersIDFromToken(token), "Adding a new chat didn't work", null);
            return null;
        }

        l().info("Successfully added the chat for the person " + persid);

        return null;
    }

    private Map updateChat(String token, String message, String persid, String chatid) {

        Map<String, Object> map = new HashMap<>();
        map.put("persid2_user", persid);
        map.put("unreadmessages", 1 + Integer.parseInt(getChat(token, null, persid).get(0).get("unreadmessages").toString()));
        map.put("messagepreview", (message.length() > 30 ? message.substring(0, 30) : message) + "...");

        //update the chat preview
        if (!casDB.Update("chat", map, "chatid=" + chatid)) {
            addUserError(getPersIDFromToken(token), "Updating the chat didn't work", null);
            return null;
        }

        l().info("Successfully updated the chat ");

        return null;
    }

    public Map<String, Object> editMessage(String token, String message, String id) {

        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("editedtimestamp", LocalDateTime.now());

        //update a message
        if (!casDB.Update("message", map, "messageid=" + id)) {
            addUserError(getPersIDFromToken(token), "Updating the message didn't work", null);
            return null;
        }

        l().info("Successfully edited the message " + id);

        return null;
    }

    private Map addSeenMessage(String token, String chatid) {
        Map<String, Object> map = new HashMap<>();
        map.put("unreadmessages", -1 + Integer.parseInt(getChat(token, chatid, null).get(0).get("unreadmessages").toString()));
        map.put("messagepreview", "");

        //change the message preview
        if (!casDB.Update("chat", map, "chatid=" + chatid)) {
            addUserError(getPersIDFromToken(token), "Updating the unreadmessages of the chat didn't work", null);
            return null;
        }

        l().info("Successfully updated the unreadmessages of the chat " + chatid);

        return null;
    }

    public List<Map<String, Object>> getMessages(String token, String id, String chatid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (id == null) {
            addUserError(getPersIDFromToken(token), "The id is not valid", null);
            return null;
        }

        String[] cols = new String[]{"chatid", "messageid", "message", "createdTimestamp", "fileid", "editedTimestamp"};

        //get all chats
        List<Row> entry = casDB.Select("message", cols, id != null ? "messageid=" + id : "chatid=" + chatid, 0, null, null).all();

        List<Map<String, Object>> list = new LinkedList<>();

        if (list.size() > 0) {
            l().info("Fetching the messages were successful");
        } else {
            l().info("The message was not found");
            return null;
        }

        //rename the keys
        for (Row r : entry) {
            Map<String, Object> map = new HashMap<>();

            for (int i = 0; i < cols.length; i++) {
                map.put(cols[i], r.getObject(i).toString());
            }

            list.add(map);
            addSeenMessage(token, map.get("chatid").toString());
        }

        return list;
    }

    public Map<String, Object> addMessage(String token, String message, String persid, String fileid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (!fileExists(fileid)) {
            addUserError(getPersIDFromToken(token), "The file does not exist", fileid);
            return null;
        }

        String chatid = getChat(token, null, persid).get(0).get("chatid").toString();

        //add the chat if it doesn't exist
        if (!chatExists(getPersIDFromToken(token), persid)) {
            addChat(token, persid, message);
        } else {
            updateChat(token, message, persid, chatid);
        }


        String uuid = casDB.getRandomUUID();

        Map<String, Object> map = new HashMap<>();
        map.put("chatid", chatid);
        map.put("messageid", uuid);
        map.put("message", message);
        map.put("createdtimestamp", LocalDateTime.now());
        map.put("fileid", fileid);
        map.put("editedtimestamp", null);

        //insert the new chat message
        if (!casDB.Insert("message", map)) {
            addUserError(getPersIDFromToken(token), "Could not add the message", null);
            return null;
        }
        map.put("id", chatid);

        l().info("The message with the id " + uuid + " was successful added");

        return map;
    }

    public Map<String, Object> deleteMessage(String token, String id) {
        if (!casDB.isUUID(id)) {
            addUserError(null, "The id is not valid", id);
            return null;
        }

        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);

        //delete the message
        if (!casDB.Delete("message", "messageid=" + id)) {
            addUserError(getPersIDFromToken(token), "Neo: Could not remove the message " + id, id);
            return null;
        }

        return map;
    }
}
