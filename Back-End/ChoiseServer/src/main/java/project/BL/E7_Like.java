package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import java.util.*;

public class E7_Like extends E6_Comment {

    public List<Map<String, Object>> getLikes(String token, String id, String type) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (id == null || type == null) {
            addUserError(getPersIDFromToken(token), "The id or the type are null", null);
            return null;
        }

        //check what kind of like it is
        if (!new HashSet<String>(Arrays.asList(new String[]{"comment", "post", "challenge"})).contains(type)) {
            addUserError(getPersIDFromToken(token), "The type is not valid. It has to be 'post', 'challenge' or 'comment'", type);
            return null;
        }

        id = id != null ? "{id:'" + id + "'}" : "";
        List<Map<String, Object>> list = neoDB.Match("(e1:" + type + id + ")-[c:likes]->()", null, null, "e1.id " + type + "id, c.id id, c.type chind", null, null, null);

        if (list.size() > 0) {
            l().info("Fetching the likes were successful");
        } else {
            l().info("The like was not found");
        }

        return list;
    }

    public Map<String, Object> addLike(String token, String chind, String id, String type) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (type.equals("comment")) {
            if (!commentExists(id)) {
                addUserError(getPersIDFromToken(token), "The comment does not exist", id);
                return null;
            }
        } else if (type.equals("post")) {
            if (!postExists(id)) {
                addUserError(getPersIDFromToken(token), "The post does not exist", id);
                return null;
            }
        } else if (type.equals("challenge")) {
            if (!challengeExists(id)) {
                addUserError(getPersIDFromToken(token), "The challenge does not exist", id);
                return null;
            }
        } else {
            addUserError(getPersIDFromToken(token), "The type is not valid. It has to be 'post', 'challenge' or 'comment'", type);
            return null;
        }

        type = Character.toUpperCase(type.charAt(0)) + type.substring(1);
        String uuid = casDB.getRandomUUID();

        if (!neoDB.Create("(e1:User{id:'" + getPersIDFromToken(token) + "'}), (e2:" + type + "{id:'" + id + "'})", null, "(e1)-[:likes{id:'" + uuid + "', chind:'" + chind + "'}]->(e2)", null, null)) {
            addUserError(getPersIDFromToken(token), "Could not add the like", null);
            return null;
        }

        l().info("The " + type + " like " + uuid + " was successful added");

        Map<String, Object> map = new HashMap<>();
        map.put("id", uuid);
        map.put("chind", chind);
        map.put(type.toLowerCase() + "id", id);

        return map;

    }

    public Map<String, Object> deleteLike(String token, String id) {
        if (!casDB.isUUID(id)) {
            addUserError(null, "The persid is not valid", id);
            return null;
        }

        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);

        if (!neoDB.Delete("(e:User)-[c:likes{id:'" + id + "'}]->()", false, "c", null)) {
            addUserError(getPersIDFromToken(token), "Neo: Could not remove the like " + id, id);
            return null;
        }

        return map;
    }

}
