
package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.BL.Encryption.EncryptionAccess;
import project.DAL.Cassandra.CassandraAccess;
import project.DAL.Neo4j.Neo4jAccess;

public class E0_Ground {

    protected CassandraAccess casDB = CassandraAccess.getInstance();
    protected Neo4jAccess neoDB = Neo4jAccess.getInstance();
    protected EncryptionAccess enc = new EncryptionAccess();
    private Logger logger;
    private String lastclass;

    public Logger l() {
        String currentclass = Thread.currentThread().getStackTrace()[2].getClassName();
        if (lastclass == null || (!lastclass.equals(currentclass))) {
            logger = LoggerFactory.getLogger(currentclass);
            lastclass = currentclass;
        }
        return logger;
    }

    protected void NEOcreateIfNotExists(String[] match, String returnval[]) {
        if (match.length != returnval.length) {
            try {
                throw new Exception("NEOcreateIfNotExists: Bought arrays need to have the same amount of variables");
            } catch (Exception e) {
                logger.error(e.getMessage(), e.getCause());
            }
        }

        for (int i = 0; i < match.length; i++) {
            NEOcreateIfNotExists(match[i], returnval[i], null);
        }
    }

    protected void NEOcreateIfNotExists(String match, String returnval) {
        NEOcreateIfNotExists(match, returnval, null);
    }

    protected void NEOcreateIfNotExists(String match, String returnval, String creationString) {
        if (neoDB.Match(match, null, null, returnval, null, null, null).size() == 0) {

            match = (creationString != null &&
                    !creationString.trim().isEmpty()) ?
                    creationString : match;

            neoDB.Create(null, null, match, returnval, null);
        }
    }

}
