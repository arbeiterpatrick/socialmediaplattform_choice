package project.BL.GeoIP;


import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
public class GeoIPAccess {

    private String ip = null;

    private String country;
    private String city;
    private String postal;
    private String state;
    private Double latitude;
    private Double longitude;
    private Logger l = LoggerFactory.getLogger(this.getClass().getName());

    String path = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "java"
            + File.separator + "BL/GeoIP" + File.separator + "GeoLite2-City.mmdb";

    public GeoIPAccess(String ip) {
        l.info("This product includes GeoLite2 data created by MaxMind, available from\n" +
                "<a href=\"http://www.maxmind.com\">http://www.maxmind.com</a>.");
        this.ip = ip;
        init();
    }

    private void init() {

        File database = new File(path);
        DatabaseReader dbReader = null;
        try {
            //read in the database
            dbReader = new DatabaseReader.Builder(database).build();

            //get information of the ip
            InetAddress ipAddress = InetAddress.getByName(ip);
            CityResponse response = dbReader.city(ipAddress);

            //save the information
            country = response.getCountry().getName();
            city = response.getCity().getName();
            postal = response.getPostal().getCode();
            state = response.getLeastSpecificSubdivision().getName();
            latitude = response.getLocation().getLatitude();
            longitude = response.getLocation().getLongitude();

        } catch (IOException | GeoIp2Exception e) {
            l.warn(e.getMessage());
        }
    }

    public String getIp() {
        return ip;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getPostal() {
        return postal;
    }

    public String getState() {
        return state;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
