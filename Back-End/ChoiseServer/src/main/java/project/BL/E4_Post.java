package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.datastax.driver.core.Row;

import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class E4_Post extends E3_File {
    public boolean postExists(String id) {
        return casDB.isUUID(id) && casDB.Select("post", new String[]{"postid"}, "postid=" + id, 0, null, null).all().size() > 0;
    }

    protected void NEOsetHashtags(String postOrComment, String id, String text) {
        postOrComment = postOrComment.toLowerCase().contains("post") ? "Post" : "Comment";

        //find  all hashtags
        Matcher mat = Pattern.compile("#(\\S+)").matcher(text);

        while (mat.find()) {
            String hashtagname = mat.group(1);

            //write the hashtasg into the database
            NEOcreateIfNotExists("(r:Hashtag{name:'" + hashtagname + "'})", "r");
            NEOcreateIfNotExists("(r:" + postOrComment + "{id:'" + id + "'})", "r");

            //link the hashtags with the post or comment
            neoDB.CreateAsync("(r2:Hashtag{name:'" + hashtagname + "'}),(r3:" + postOrComment + "{id:'" + id + "'})", null, "(r3) -[c:has_hashtag]->(r2)", "r3", null);
        }
    }

    protected void NEOremoveHashtags(String postOrComment, String id) {
        postOrComment = postOrComment.toLowerCase().contains("post") ? "Post" : "Comment";

        //remove a hashtag
        neoDB.DeleteAsync("(r:" + postOrComment + "{id:" + id + "})-[c:has_hashtag]->(r2:hashtag)", false, "c", null);
    }

    public Map<String, Object> addPost(String token, String message, String viewmode, String fileid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
            return null;
        }

        if (fileid != null && !fileExists(fileid)) {
            addUserError(getPersIDFromToken(token), "The fileid is not valid", fileid);
            return null;
        }

        int mode = -1;

        //check the mode
        try {
            mode = Integer.parseInt(viewmode);
            if (mode < 0) {
                throw new Exception();
            }

        } catch (Exception ex) {
            addUserError(getPersIDFromToken(token), "The viewmode is not valid.", viewmode);
        }


        Map<String, Object> map = new HashMap<>();
        map.put("postid", casDB.getRandomUUID());
        map.put("persid_user", getPersIDFromToken(token));
        map.put("fileid_file", fileid);
        map.put("createdtimestamp", LocalDateTime.now());
        map.put("changedtimestamp", null);
        map.put("message", message);
        map.put("viewmode", mode);

        //insert the post in cassandra
        if (!casDB.Insert("post", map)) {
            addUserError(getPersIDFromToken(token), "Could not add the post", map.get("postid").toString());
            return null;
        }

        //insert the post in neo4j
        if (!neoDB.Create(null, null, "(e1:Post{id:'" + map.get("postid").toString() + "'})", "e1", null)) {
            addUserError(getPersIDFromToken(token), "Could not add the post in DB2", map.get("postid").toString());
            return null;
        }

        //write the hashtag into the database
        NEOsetHashtags("post", map.get("postid").toString(), message);

        l().info("Adding the post " + map.get("postid").toString() + " was successful");

        map.put("id", map.get("postid").toString());
        map.put("fileid", fileid);
        map.put("persid", getPersIDFromToken(token));
        map.remove("postid");
        map.remove("persid_user");
        map.remove("fileid_file");

        return map;

    }

    public Map<String, Object> editPost(String token, String postid, String message, String viewmode, String fileid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        if (!postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The post doesn't exist", postid);
            return null;
        }

        if (fileid != null && !fileExists(fileid)) {
            addUserError(getPersIDFromToken(token), "The fileid is not valid", fileid);
            return null;
        }

        int mode = -1;

        //check viewmode
        try {
            mode = Integer.parseInt(viewmode);
            if (mode < 0) {
                throw new Exception();
            }

        } catch (Exception ex) {
            addUserError(getPersIDFromToken(token), "The viewmode is not valid.", viewmode);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("fileid_file", fileid);
        map.put("changedtimestamp", LocalDateTime.now());
        map.put("viewmode", mode);
        map.put("message", message);

        //update the post
        if (!casDB.Update("post", map, "postid=" + postid)) {
            addUserError(getPersIDFromToken(token), "Could not update the post", postid);
            return null;
        }

        l().info("Updating the post " + postid + " was successful");

        //renew all hashtag connections
        NEOremoveHashtags("post", postid);
        NEOsetHashtags("post", postid, message);

        map.put("id", postid);
        map.put("fileid", fileid);
        map.put("persid", getPersIDFromToken(token));
        map.remove("postid");
        map.remove("persid_user");
        map.remove("fileid_file");

        return map;

    }

    public Map<String, Object> deletePost(String token, String postid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("postid", postid);

        if (!postExists(postid)) {
            return map;
        }

        //delete the post
        if (!casDB.Delete("post", "postid=" + postid)) {
            addUserError(getPersIDFromToken(token), "Could not delete the post", postid);
            return null;
        }

        //delete the connections to the post
        neoDB.DeleteAsync("(e1:Post{id:'" + postid + "'})", true, "e1", null);

        l().info("Deleting the post " + postid + " was successful");

        //remove all hashtags for this post
        NEOremoveHashtags("post", postid);

        return map;
    }

    public List<Map<String, Object>> getPosts(String token, String postid, String persid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        if (postid != null && !postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The post does not exist", postid);
            return null;
        }

        if (postid != null && !userExists(persid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", persid);
            return null;
        }

        List<Map<String, Object>> list = new LinkedList<>();
        String where = postid != null && !postid.trim().isEmpty() ? "postid=" + postid : (persid == null ? "" : "persid_user=" + persid);

        //fetch from the db
        List<Row> rows = casDB.Select("post", new String[]{"postid", "persid_user", "message", "fileid_file", "createdtimestamp", "changedtimestamp", "viewmode"}, where, 0, null, null).all();

        String[] calls = new String[]{"id", "byuser", "message", "fileid_file", "createdtimestamp", "changedtimestamp", "viewmode"};

        //rename the keys of the returned values
        for (Row r : rows) {
            Map<String, Object> map = new HashMap<>();

            for (int i = 0; i < calls.length; i++) {
                map.put(calls[i], r.getObject(i) == null ? null : r.getObject(i).toString());
            }

            map.put("viewmode", r.getInt("viewmode"));

            Map<String, Object> user = getUsers(map.get("byuser").toString(), token, false, null, null).get(0);
            map.put("firstname", user.get("firstname"));
            map.put("surname", user.get("surname"));

            list.add(map);
        }


        Collections.sort(list, (Comparator<Map<String, Object>>) (m1, m2) -> -m1.get("createdtimestamp").toString().compareTo(m2.get("createdtimestamp").toString()));


        l().info("The posts were successful fetched from the database.");

        return list;

    }


}
