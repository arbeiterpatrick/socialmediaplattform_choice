
package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import graphql.GraphQLException;
import project.BL.GeoIP.GeoIPAccess;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class E1_Logs extends E0_Ground {

    private String ip;

    public void setIP(String ip) {
        this.ip = ip;
    }

    protected String getIP() {
        return ip;
    }

    protected void NEOLoginFrom(String persid, boolean successful) {
        GeoIPAccess ip = new GeoIPAccess(getIP());

        //log the ip with the location into neo4j
        NEOcreateIfNotExists(new String[]{
                "(e2:IP{id:'" + getIP() + "'})",
                "(e1:Location{name:'" + ip.getCountry() + "_" + ip.getState() + "_" + ip.getCity() + "',state:'" + ip.getState() + "',city:'" + ip.getCity() + "',country:'" + ip.getCountry() + "',latitude:" + ip.getLatitude() + ",longitude:" + ip.getLongitude() + "})",
                "(e2:IP{id:'" + getIP() + "'}) -[c1:belongs_to]->(e1:Location{name:'" + ip.getCountry() + "_" + ip.getState() + "_" + ip.getCity() + "'})",
        }, new String[]{"e2", "e1", "c1"});

        //connect the ip with a user
        neoDB.CreateAsync("(e2:IP{id:'" + getIP() + "'}), (e1:User{id:'" + persid + "'})", null, "(e1) -[c:logs_in_from{successful:" + successful + ", timestamp:'" + LocalDateTime.now() + "'}]->(e2)", "c", null);

        l().info("Log the ip in the Neo4j database");

    }

    public void addAccessAttempt(String header, String query, String user) {

        //check if it has to be encrypted because it contains a password
        String[] items = new String[]{"loginUser", "addUser", "updateUser", "deleteUser"};
        boolean fixenc = Arrays.stream(items).anyMatch(query::contains);

        //build the logging contend
        Map<String, Object> map = new HashMap<>();
        map.put("logid", casDB.getRandomUUID());
        map.put("access_header", header);
        map.put("access_query", fixenc ? enc.encrypt(query) : query);
        map.put("loggedin_user", user);
        map.put("encrypted", fixenc);
        map.put("added", LocalDateTime.now());

        //insert
        casDB.InsertAsync("accesslog", map);

        l().info("Write Accesslog for a request  of the user " + user);
    }

    public void addLoginLog(String uuid, String user) {
        Map<String, Object> map = new HashMap<>();
        map.put("usertoken", uuid);
        map.put("username", user);
        map.put("ip", ip);
        map.put("added", LocalDateTime.now());

        casDB.InsertAsync("loggedinUser", map);

        l().info("User " + user + " logged in from " + ip);
        ip = null;
    }

    public void addUserAction(String user, String action, boolean permitted) {
        Map<String, Object> map = new HashMap<>();
        map.put("logid", casDB.getRandomUUID());
        map.put("loggedin_user", user);
        map.put("added", LocalDateTime.now());
        map.put("action", action);
        map.put("permitted", permitted);

        casDB.InsertAsync("useractionlog", map);

        l().info("User " + user + " tried to access the permission " + action + ". It was " + (permitted ? "" : "NOT ") + "permissible");
    }

    public void addUserError(String user, String errormessage, String error_creating_value) {
        addUserError(user, errormessage, error_creating_value, true);
    }

    public void addUserError(String user, String errormessage, String error_creating_value, boolean throwError) {
        //Reason behind the userErrorLog is, if the platform is attacked, it can be easier seen
        Map<String, Object> map = new HashMap<>();
        map.put("logid", casDB.getRandomUUID());
        map.put("loggedin_user", user);
        map.put("added", LocalDateTime.now());
        map.put("error", errormessage);
        map.put("error_creating_value", error_creating_value);

        casDB.InsertAsync("usererrorlog", map);

        l().warn(errormessage + ", Value: " + error_creating_value);
        l().info("An error of User " + user + " was logged");

        if (throwError) {
            throw new GraphQLException(errormessage);
        }
    }

    public String getRandomUUID() {
        return casDB.getRandomUUID();
    }

}
