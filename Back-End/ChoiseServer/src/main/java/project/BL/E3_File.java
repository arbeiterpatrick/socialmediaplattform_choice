package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class E3_File extends E2_User {

    public boolean fileExists(String id) {
        if (id == null || id.isEmpty()) {       //to allow actions where the file is also valid when it is not added e.g. addUser
            return true;
        }

        return casDB.isUUID(id) && casDB.Select("file", new String[]{"fileid"}, "fileid=" + id, 0, null, null).all().size() > 0;
    }

    public Map<String, Object> deleteFile(String token, String id) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);

        if (!fileExists(id)) {
            l().info("The file is already deleted");
            return map;
        }

        if (!casDB.Delete("file", "id=" + id)) {
            addUserError(getPersIDFromToken(token), "The file " + id + " could not be deleted", null);
            return null;
        }

        l().info("The file with the id " + id + " was successful deleted");

        return map;
    }


    private String[] getFileInfo(String input) {
        // get infos about the file
        // 64 encodes data like "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAKAC"

        try {
            ByteArrayInputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(input.split("[,]")[1]));

            String mimeType = URLConnection.guessContentTypeFromStream(is); //mimeType is something like "image/jpeg"
            String fileExtension = mimeType.split("[/]")[1];    //fileextention
            String size = is.toString().length() + "";

            return new String[]{mimeType, fileExtension, size};
        } catch (IOException ioException) {
            return null;
        }
    }

    public Map<String, Object> addFile(String token, String name, String contend) {

        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
        }

        //get new uuid
        String uuid = casDB.getRandomUUID();
        //get fileinfo
        String[] info = this.getFileInfo(contend);

        if (info == null) {
            addUserError(null, "The file is corrupted try again", token);
        }

        //prepare data for the database
        Map<String, Object> map = new HashMap<>();
        map.put("fileid", uuid);
        map.put("contend", contend);
        map.put("name", name + "." + info[1]);
        map.put("type", info[0]);
        map.put("size", info[2]);
        map.put("createdtimestamp", LocalDateTime.now());

        //insert file
        if (casDB.Insert("file", map)) {
            l().info("The file " + name + " with the id " + uuid + " was successful added");

            return getFile(token, uuid).get(0);
        }

        l().warn("The file " + name + " could not be added");

        return null;
    }


    public List<Map<String, Object>> getUserBackgroundimage(String token, String firstname, String surname) {
        return getFile(token, getUsers(getPersIDFromToken(token), token, false, firstname, surname).get(0).get("background").toString());
    }


    public List<Map<String, Object>> getUserProfilepicture(String token, String firstname, String surname) {
        return getFile(token, getUsers(getPersIDFromToken(token), token, false, firstname, surname).get(0).get("profile").toString());
    }


    public List<Map<String, Object>> getFile(String token, String id) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
        }

        if (!fileExists(id)) {
            addUserError(null, "The file does not exist", token);
        }

        //return the data
        return casDB.Select("file", new String[]{"fileid AS id", "size", "name", "type", "contend AS base64contend", "createdtimestamp"}, "fileid=" + id, 1, null, null,
                new String[]{"id", "size", "name", "type", "base64contend", "createdtimestamp"});
    }
}
