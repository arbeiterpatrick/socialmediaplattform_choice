package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class E6_Comment extends E5_Challenge {

    public boolean commentExists(String commentid) {
        return casDB.isUUID(commentid) && casDB.Select("comment", new String[]{"commentid"}, "commentid=" + commentid, 0, null, null).all().size() > 0;

    }

    public List<Map<String, Object>> getComments(String token, String postid, String persid, String commentid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        if (postid == null && persid == null && commentid == null) {
            addUserError(getPersIDFromToken(token), "At least one filter has to be set.", null);
            return null;
        }

        if (postid != null && !postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The post does not exist", postid);
            return null;
        }

        if (persid != null && !userExists(persid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", postid);
            return null;
        }

        //prepare the where clause
        String where = "postid=" + postid + (persid != null && !persid.trim().isEmpty() ? " AND persid=" + persid : "");
        where = commentid != null && !commentid.trim().isEmpty() ? "commentid=" + commentid : where;

        //get the comments
        List<Map<String, Object>> list = casDB.Select("comment", new String[]{"commentid", "postid_post", "persid_user", "editedTimestamp", "createdTimestamp", "message"}, where, 0, null, null, new String[]{"commentid", "postid", "persid", "editedTimestamp", "createdTimestamp", "message"});

        l().info("The comments were successfully fetched.");

        return list;


    }

    public Map<String, Object> addComment(String token, String message, String postid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
            return null;
        }

        if (!postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The postid is not valid", postid);
            return null;
        }

        String uuid = casDB.getRandomUUID();

        Map<String, Object> map = new HashMap<>();
        map.put("commentid", uuid);
        map.put("postid_post", postid);
        map.put("persid_user", getPersIDFromToken(token));
        map.put("createdTimestamp", LocalDateTime.now());
        map.put("changedTimestamp", null);
        map.put("message", message);

        //insert the new post
        if (!casDB.Insert("comment", map)) {
            addUserError(getPersIDFromToken(token), "Could not add the comment to the post" + postid, null);
            return null;
        }

        //add the hashtag to the comment
        NEOsetHashtags("comment", uuid, message);

        l().info("Adding the comment to the post " + postid + " was successful");

        map.put("persid", map.get("persid_user"));
        map.put("postid", postid);
        map.remove("postid_post");
        map.remove("persid_user");

        return map;

    }

    public Map<String, Object> editComment(String token, String commentid, String message) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        if (!commentExists(commentid)) {
            addUserError(getPersIDFromToken(token), "The comment doesn't exist", commentid);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("changedTimestamp", LocalDateTime.now());
        map.put("message", message);

        //update the comment
        if (!casDB.Update("comment", map, "commentid=" + commentid)) {
            addUserError(getPersIDFromToken(token), "Could not update the comment", commentid);
            return null;
        }

        l().info("Updating the comment " + commentid + " was successful");

        //renew the hashtags
        NEOremoveHashtags("comment", commentid);
        NEOsetHashtags("comment", commentid, message);

        return getComments(token, null, null, commentid).get(0);
    }

    public Map<String, Object> deleteComment(String token, String commentid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("commentid", commentid);

        if (!commentExists(commentid)) {
            return map;
        }

        //remove the comment
        if (!casDB.Delete("comment", "commentid=" + commentid)) {
            addUserError(getPersIDFromToken(token), "Could not delete the comment", commentid);
            return null;
        }

        l().info("Deleting the comment " + commentid + " was successful");

        //remove the hashtags
        NEOremoveHashtags("comment", commentid);

        return map;
    }
}
