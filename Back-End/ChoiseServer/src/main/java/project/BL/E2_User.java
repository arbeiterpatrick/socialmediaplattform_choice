
package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.datastax.driver.core.Row;
import org.slf4j.LoggerFactory;
import project.Util.Util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author Tower
 */
public class E2_User extends E1_Logs {

    private LocalDateTime stamp = LocalDateTime.now().plusMinutes(Util.pauseBetweenRunningSessionLogoutCron);
    private Map<String, LinkedList<String>> cachedUserpermissions = new HashMap<>();      //string value=token
    private Map<String, LocalDateTime> cachedUsertimestamps = new HashMap<>();

    public E2_User() {
        //check if the adminuser exists
        if (casDB.Select("user", new String[]{"persid"}, "email='" + Util.adminemail + "'", 0, null, null).all().size() == 0) {
            try {
                //add the adminuser
                addUser(
                        Util.adminemail, Util.adminpassword,
                        "Admin", "", "",
                        "", LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")), "",
                        "", "", 0);
            } catch (Exception e) {
                LoggerFactory.getLogger(this.getClass().getName()).error("Error at creating the admin user.", e);
            }
        }
    }

    public boolean isLoggedIn(String token) {
        //check if the token is a valid uuid
        if (!casDB.isUUID(token)) {
            addUserError("null", "The token is not valid", token, false);
            return false;
        }
        //check if uuid belongs to a logged in user
        return casDB.Select("user", new String[]{"persid"}, "session_token=" + token, 0, null, null).all().size() > 0;

    }

    public String loginUser(String email, String password, String token) {
        if (!isEmail(email)) {
            addUserError(getPersIDFromToken(token), "The given email is not valid", email);
            return null;
        }

        //if a old token is set from a previous session, that one will be used
        //if no token is set use the null value
        token = token == null ? null : token.replace("Bearer", "");

        //check if the token is valid
        if (token != null && (!token.isEmpty()) && !casDB.isUUID(token)) {
            addUserError("null", "The token is not valid", token);
            return null;
        }

        //check if already loggedin
        if (token != null && (!token.isEmpty()) && isLoggedIn(token)) {
            addLoginLog(token, email);
            l().info("The user " + email + "is already loggedin");
            return token;
        }

        //get new token
        String uuid = casDB.getRandomUUID();

        //check for some errors
        List<Row> x = casDB.Select("user", new String[]{"persid"}, "email = '" + email + "' AND password = '" + enc.encrypt(password) + "'", 0, null, null).all();
        if (x.size() > 1) {
            NEOLoginFrom(uuid, false);

            l().warn("Error: The User exists more than one time.");
            return null;
        } else if (x.size() != 1) {
            NEOLoginFrom(uuid, false);

            addUserError(null, "The user with this password doesn't exist or has wrong login credentials", token);
            return null;
        }

        //prepare for the login
        String persid = x.get(0).getObject("persid").toString();

        Map<String, Object> map = new HashMap<>();
        map.put("session_token", uuid);
        map.put("session_timestamp", LocalDateTime.now());

        //login user via update in database
        if (!casDB.Update("user", map, "persid=" + persid)) {
            l().warn("Login of the user " + email + " didn't work");
            return null;
        }

        //create a cache with the loggedin user timestamps and permissions
        cachedUsertimestamps.put(uuid, LocalDateTime.now());
        getUserPermissions(token);
        l().info("Set the session timestamp of the user " + email + " to now");

        //log from where the user loggedin
        NEOLoginFrom(uuid, true);

        //log login
        addLoginLog(uuid, email);
        l().info("Logged in the user " + email);
        return uuid;
    }

    public boolean logoutUser(String email, String token) {
        if (!isEmail(email)) {
            addUserError(getPersIDFromToken(token), "The given email is not valid", email);
            return false;
        }
        if (!casDB.isUUID(token)) {
            addUserError("null", "The token is not valid", token);
        }
        List<Row> rows = casDB.Select("user", new String[]{"persid"}, "email ='" + email + "' AND session_token=" + token, 0, null, null).all();

        Map<String, Object> map = new HashMap<>();
        map.put("session_token", null);
        map.put("session_timestamp", null);


        for (Row r : rows) {
            if (!casDB.Update("user", map, "persid=" + r.getUUID("persid"))) {
                cachedUserpermissions.remove(token);
                l().warn("Logout of user " + email + " didn't work");
                return false;
            }
        }
        l().info("Logged out the user " + email);

        return true;
    }

    public Map<String, Object> getUserInfo(String token) {
        if (!isLoggedIn(token)) {
            return null;
        }

        Row r = casDB.Select("user", new String[]{"persid", "email"}, "session_token=" + token, 0, null, null).one();
        Map<String, Object> map = new HashMap<>();

        map.put("token", token);
        map.put("operation_successful", true);
        map.put("email", r.getString("email"));
        map.put("persid", r.getObject("persid").toString());

        l().info("Get infos about the user " + map.get("email"));

        return map;
    }

    /**
     * @param token
     * @return Is null when the token is null or the user was not found
     */
    public String getPersIDFromToken(String token) {
        if (token == null || token.isEmpty()) {
            return null;
        }

        //get persid from the database
        List<Row> list = casDB.Select("user", new String[]{"persid"}, "session_token=" + token, 0, null, null).all();
        return list.size() == 0 ? null : list.get(0).getObject("persid").toString();
    }

    private void setTimespampToNow(String token) {

        if (token != null && !token.isEmpty()) {

            if (cachedUsertimestamps.get(token) == null || cachedUsertimestamps.get(token).isAfter(LocalDateTime.now().minusMinutes(3))) {

                //check if uuid is valid
                if (!casDB.isUUID(token)) {
                    addUserError("null", "The token is not valid", token);
                }

                //prepare for database
                Map<String, Object> map = new HashMap<>();
                map.put("session_timestamp", LocalDateTime.now().plusMinutes(Util.AbsentSessionTime));

                //set timestamp
                if ((!isLoggedIn(token)) || (!casDB.Update("user", map, "persid=" + getPersIDFromToken(token)))) {
                    l().warn("Could not update the sessiontime of the user with the session token " + token);
                } else {
                    l().info("Updated the session timestamp of the user with the session token " + token);
                    cachedUsertimestamps.put(token, LocalDateTime.now());
                }
            }
        }
    }

    private void CronjobSesstionAutoLogout() {
        if (stamp.isAfter(LocalDateTime.now())) {//check if its time to run this cronjob
            l().info("Run Cronejob SessionAutoLogout");

            List<Row> rows = casDB.Select("user", new String[]{"email", "persid"}, "session_timestamp <" + LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(1)), 0, null, null).all();

            Map<String, Object> map = new HashMap<>();
            map.put("session_token", null);
            map.put("session_timestamp", null);

            //remove all outdated sessiontokens
            for (Row r : rows) {
                if (!casDB.Update("user", map, "persid=" + r.getUUID("persid"))) {
                    l().warn("Logout of the user " + r.getString("email") + " didn't work");
                } else {
                    l().info("Loggged the user " + r.getString("email") + " out.");
                }
            }

            stamp = LocalDateTime.now().plusMinutes(Util.pauseBetweenRunningSessionLogoutCron);
        }
    }

    public LinkedList<String> getUserPermissions(String token) {

        String[] defaultlist = new String[]{
                "login",
        };
        String[] list = new String[]{
                "get_comments_by_filter",
                "get_login_info",
                "login",
                "logout"
        };

        //use default permissions when the user is not loggedin
        if (token == null || token.isEmpty()) {    //load default permissions
            list = defaultlist;
        } else if (!casDB.isUUID(token)) {
            addUserError("null", "The token is not valid", token);
        }

        cachedUserpermissions.put(token, new LinkedList<>(Arrays.asList(list)));
        return cachedUserpermissions.get(token);
    }

    public boolean accessAllowed(String token, String permission) {
        //if permission is not in list, it is forbidden
        boolean permitted;

        token = token == null ? null : token.replace("Bearer", "");


        if (cachedUserpermissions.get(token) == null) {       //load permissions        //also if user is not loggedin
            cachedUserpermissions.put(token, getUserPermissions(token));
        }

        permitted = cachedUserpermissions.get(token) != null && cachedUserpermissions.get(token).contains(permission);
        addUserAction(getPersIDFromToken(token), permission, permitted);
        setTimespampToNow(token);
        CronjobSesstionAutoLogout();

//        return permitted;
        return true;        //is temporally true because the webinterface for the config of the permissions is missing
    }

    public Map<String, Object> editUser(String persid, String token, String firstname, String surname, String aboutme, String residence, String birthday, String employment, String fontface, String backgroundcolor, int fontsize) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
        }

        if (!userExists(persid)) {
            addUserError(null, "The user does not exist", persid);
        }

        LocalDate date;

        //parse birthday
        try {
            date = LocalDate.parse(birthday, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        } catch (Exception ex) {
            addUserError(null, "The birthday needs to have a valid format(dd-MM-yyyy).", birthday);
            return null;
        }

        //prepare for database
        Map<String, Object> map = new HashMap<>();
        map.put("firstname", firstname);
        map.put("surname", surname);
        map.put("challenge_points", 0);
        map.put("about_me", aboutme);
        map.put("residence", residence);
        map.put("birthday", date);
        map.put("employment", employment);
        map.put("fontface", fontface);
        map.put("backgroundcolor", backgroundcolor);
        map.put("fontsize", fontsize);

        //update user in database
        if (!casDB.Update("user", map, "persid=" + persid)) {
            addUserError(null, "The user " + persid + "could not be added", null);
            return null;
        }
        l().info("The user " + persid + " was successful updated");

        return map;
    }

    public Map<String, Object> addUser(String email, String password, String firstname, String surname, String aboutme, String residence, String birthday, String employment, String fontface, String backgroundcolor, int fontsize) {
        if (!isEmail(email)) {
            addUserError(null, "The given email is not valid", email);
            return null;
        }

        List<Row> x = casDB.Select("user", new String[]{"persid"}, "email = '" + email + "' AND password = '" + enc.encrypt(password) + "'", 0, null, null).all();

        //check if the user exists already
        if (x.size() > 0) {
            addUserError(null, "The user exists already", email);
            return null;
        }

        long date = 0;

        if (birthday.trim().isEmpty()) {
            addUserError(null, "The birthday is not allowed to be empty", birthday);
            return null;
        }

        //parse date
        try {
            if (birthday != null) {
                LocalDate dd = LocalDate.parse(birthday, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                date = dd.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            }
        } catch (Exception ex) {
            addUserError(null, "The birthday needs to have a valid format(dd-MM-yyyy).", birthday);
            return null;
        }

        //get new uuid
        String uuid = casDB.getRandomUUID();

        Map<String, Object> map = new HashMap<>();
        map.put("persid", uuid);
        map.put("email", email);
        map.put("password", enc.encrypt(password));
        map.put("firstname", firstname);
        map.put("surname", surname);
        map.put("challenge_points", 0);
        map.put("about_me", aboutme);
        map.put("residence", residence);
        map.put("birthday", date);
        map.put("employment", employment);
        map.put("fontface", fontface);
        map.put("backgroundcolor", backgroundcolor);
        map.put("fontsize", fontsize);

        //insert into cassandra
        if (!casDB.Insert("user", map)) {
            addUserError(null, "The user with the email " + email + "could not be added", null);
            return null;
        }

        //insert into neo4j
        if (!neoDB.Create(null, null, "(e:User{email:'" + email + "', id:'" + uuid + "'})", "e", null)) {
            addUserError(uuid, "Neo: Could not create the user " + uuid, uuid);
            return null;
        }
        l().info("The user with the email " + email + " was successful added");

        map.put("id", uuid);

        return map;
    }

    public Map<String, Object> deleteUser(String persid, String password, String email, String token) {
        if (!casDB.isUUID(persid)) {
            addUserError(null, "The persid is not valid", persid);
            return null;
        }

        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }


        if (!isEmail(email)) {
            addUserError(null, "The given email is not valid", email);
            return null;
        }

        List<Row> x = casDB.Select("user", new String[]{"persid"}, "email = '" + email + "' AND password = '" + enc.encrypt(password) + "' AND persid=" + persid + " AND session_token=" + token, 0, null, null).all();

        if (x.size() != 1) {
            addUserError(null, "The email or the password or the persid or the session token is wrong", email);
            return null;
        }

        //delete user from casandra db
        if (!casDB.Delete("user", "persid=" + x.get(0).getObject("persid").toString())) {
            addUserError(persid, "Could not delete the user " + persid, persid);
            return null;
        }

        //delete user from neo4j db
        if (!neoDB.Delete("(e:User{id:'" + persid + "'})", true, "e", null)) {
            addUserError(persid, "Neo: Could not delete the user " + persid, persid);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("email", email);

        return map;

    }

    public List<Map<String, Object>> getUsers(String persid, String token, boolean current, String vorname, String nachname) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
        }

        if (current) {  //get current user info
            persid = getPersIDFromToken(token);
        }

        if (persid != null && !userExists(persid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", persid);
            return null;
        }


        String[] entry = new String[]{"firstname", "surname", "email", "aboutme", "residence", "birthday", "employment", "fontface", "backgroundcolor", "persid", "permission_id", "fontsize", "challengepoints","profile","background"};
        String[] dbentry = new String[]{"firstname", "surname", "email", "about_me", "residence", "birthday", "employment", "fontface", "backgroundcolor", "persid", "permission_id", "fontsize", "challenge_points","profilepicture_fileid_file","backgroundimage_fileid_file"};
        List<Row> rows = casDB.Select("user", dbentry, persid == null ? null : ("persid=" + persid), 0, null, null).all();

        List<Map<String, Object>> list = new LinkedList<>();
        //rename the database result for the api
        for (Row r : rows) {
            Map<String, Object> map = new HashMap<>();

            int i = 0;
            while (i < entry.length - 2) {
                map.put(entry[i], r.getObject(i) == null ? null : r.getObject(i).toString());
                i++;
            }

            while (i < entry.length) {
                map.put(entry[i], r.getInt(i));
                i++;
            }

            list.add(map);

        }

        return list;
    }

    public Map<String, Object> editLoginCredentials(String token, String email, String persid, String newPassword) {
        if (!casDB.isUUID(token)) {
            addUserError(null, "The token is not valid", token);
            return null;
        }

        if (!isEmail(email)) {
            addUserError(getPersIDFromToken(token), "The given email is not valid", email);
            return null;
        }

        if (!userExists(persid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", persid);
            return null;
        }

        Map<String, Object> map = new HashMap<>();

        map.put("email", email);
        map.put("password", enc.encrypt(newPassword));

        //update the login credentials
        if (!casDB.Update("user", map, "persid=" + persid)) {
            l().warn("Updating of the user login credentials fo the user " + persid + " didn't work.");
        }

        map.remove("password");
        map.put("token", token);
        map.put("persid", persid);
        map.put("operation_successful", true);

        l().info("Updating of the user login credentials fo the user " + persid + " was successfull.");

        return map;
    }

    public boolean userExists(String persid) {
        //check if a user exists
        return casDB.isUUID(persid) && casDB.Select("user", new String[]{"persid"}, "persid=" + persid, 0, null, null).all().size() > 0;
    }

    public boolean isEmail(String email) {
        return Pattern.compile("^.+@.+\\..+$").matcher(email).find();
    }

    public List<Map<String, Object>> getUsersFriends(String persid, String token) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
        }

        if (!userExists(persid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", persid);
            return null;
        }

        //get friends
        List<Map<String, Object>> rows = neoDB.Match("(f:User{id:'" + persid + "'}) -[k:knows]->(f2:User)", null, null, "f2.id AS id", null, null, null);

        List<Map<String, Object>> list = new LinkedList<>();

        //get infos if every user
        for (int i = 0; i < list.size(); i++) {
            list.add(getUsers(rows.get(i).get("id").toString(), token, false, null, null).get(0));
        }

        return list;
    }

    public List<Map<String, Object>> getUserscompletedChallenge(String challid, String token) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
        }

        //get users
        List<Map<String, Object>> rows = neoDB.Match("(f:User) -[k:has]->(e2:Challenge_completed)-[c2:has]->(f2:Challenge{id:'" + challid + "'})", null, null, "f.id AS id", null, null, null);

        List<Map<String, Object>> list = new LinkedList<>();

        //get infos about the users
        for (int i = 0; i < list.size(); i++) {
            list.add(getUsers(rows.get(i).get("id").toString(), token, false, null, null).get(0));
        }

        return list;
    }

    public List<Map<String, Object>> getFriends(String token, String persid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (persid != null && !userExists(persid)) {
            addUserError(null, "The user does not exist", persid);
            return null;
        }

        //format the persid for the query
        persid = persid != null ? ("{id:'" + persid + "'}") : "";

        //get the friends
        List<Map<String, Object>> list = neoDB.Match("(e1:User{id:'" + getPersIDFromToken(token) + "'})-[c:knows]->(e2:User" + persid + ")", null, null, "e2.id persid, c.type relationshiptype", null, null, null);

        if (list.size() > 0) {
            l().info("The friends were successfully fetched");
        }
        return list;
    }

    public Map<String, Object> addFriend(String token, String persid, String type) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }

        if (!userExists(persid)) {
            addUserError(null, "The user does not exist", persid);
            return null;
        }

        //create the friend relationship in neo4j
        if (!neoDB.Create("(e1:User{id:'" + persid + "'}), (e2:User{id:'" + getPersIDFromToken(token) + "'})", null, "(e2)-[:knows{type:'" + type + "'}]->(e1)", null, null)) {
            addUserError(getPersIDFromToken(token), "Could not add the friend" + persid, persid);
            return null;
        }

        l().info("The friend " + persid + " was successful added");

        Map<String, Object> map = new HashMap<>();
        map.put("persid", persid);
        map.put("relationshiptype", type);

        return map;

    }

    public Map<String, Object> deleteFriend(String token, String persid) {
        if (!casDB.isUUID(persid)) {
            addUserError(null, "The persid is not valid", persid);
            return null;
        }

        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in.", token);
            return null;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("persid", persid);

        //remove the friend relationship in neo4j
        if (!neoDB.Delete("(e:User{id:'" + getPersIDFromToken(token) + "'})-[c:knows]->(e:User{id:'" + persid + "'})", false, "c", null)) {
            addUserError(persid, "Neo: Could not remove the friend " + persid, persid);
            return null;
        }

        l().info("The friend was successfully deleted");

        return map;
    }
}

