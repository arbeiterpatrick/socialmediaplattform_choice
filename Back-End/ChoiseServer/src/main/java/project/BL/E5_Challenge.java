package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class E5_Challenge extends E4_Post {

    public List<Map<String, Object>> getChallenges(String id, String name) {
        //build the where clause
        String where = (id != null ? "challengeid=" + id : null);
        where = where == null ? (name != null ? "name=" + id : null) : (name != null ? " AND name=" + id : "");

        //get the challenges
        ResultSet rows = casDB.Select("challenge", new String[]{"challengeid", "postid", "persid_user", "name", "teaser", "file_fileid_file", "createdtimestamp", "status", "points", "level", "popularity"}, where, 0, null, null);

        if (rows == null) {
            l().info("No challenges were found");
            return null;
        }

        List<Map<String, Object>> list = new ArrayList<>();

        //rename the keys
        for (Row r : rows.all()) {
            Map<String, Object> map = new HashMap<>();

            map.put("id", r.getObject("challengeid").toString());
            map.put("persid", r.getObject("persid_user").toString());
            map.put("name", r.getString("name"));
            map.put("teaser", r.getString("teaser"));
            map.put("file_id", r.getObject("file_fileid_file") == null ? null : r.getObject("file_fileid_file").toString());
            map.put("createdtimestamp", r.getTimestamp("createdtimestamp"));
            map.put("status", r.getInt("status"));
            map.put("points", r.getInt("points"));
            map.put("level", r.getInt("level"));
            map.put("postid", r.getObject("postid").toString());
            map.put("popularity", r.getInt("popularity"));

            list.add(map);
        }

        return list;
    }

    public boolean challengeExists(String id) {
        return casDB.isUUID(id) && casDB.Select("challenge", new String[]{"challengeid"}, "challengeid=" + id, 0, null, null).all().size() > 0;
    }

    public Map<String, Object> addChallenge(String name, String teaser, int points, int level, String token, String fileid, String postid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
        }

        if (fileid != null && !fileExists(fileid)) {
            addUserError(getPersIDFromToken(token), "The file doesn't exist", fileid);
        }

        if (!postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The post doesn't exist", postid);
        }

        if (points < 1) {
            addUserError(getPersIDFromToken(token), "The points are to low", points + "");
        }


        //get new uuid
        String uuid = casDB.getRandomUUID();

        //prepare data for the database
        Map<String, Object> map = new HashMap<>();
        map.put("challengeid", uuid);
        map.put("persid_user", getPersIDFromToken(token));
        map.put("name", name);
        map.put("teaser", teaser);
        map.put("file_fileid_file", fileid);
        map.put("postid", postid);
        map.put("createdtimestamp", LocalDateTime.now());
        map.put("status", 0);
        map.put("points", points);
        map.put("level", level);
        map.put("popularity", 0);

        //insert challenge
        if (casDB.Insert("challenge", map)) {
            //neo4j create the challenge
            neoDB.CreateAsync(null, null, "(e1:Challenge{name:'" + name + "', id:'" + uuid + "'})", "e1", null);
            map.put("id", uuid);
            map.put("postid", postid);
            map.put("fileid", fileid);
            map.put("createdbyuserid", map.get("persid_user"));

            map.remove("challengeid");
            map.remove("persid_user");
            map.remove("file_fileid_file");
            map.remove("postid");

            l().info("The challenge " + name + " with the id " + uuid + " was successful created");

            return map;
        }

        l().warn("The challenge " + name + " could not be created");

        return null;
    }

    public Map<String, Object> editChallenge(String name, String teaser, int points, int level, String token, String fileid, String postid, String challengeid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
            return null;
        }

        if (!fileExists(fileid)) {
            addUserError(getPersIDFromToken(token), "The file doesn't exist", fileid);
            return null;
        }

        if (!postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The post doesn't exist", postid);
            return null;
        }

        if (points < 1) {
            addUserError(getPersIDFromToken(token), "The points are to low", points + "");
            return null;
        }


        String uuid = casDB.getRandomUUID();

        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("teaser", teaser);
        map.put("file_fileid_file", fileid);
        map.put("postid", postid);
        map.put("createdtimestamp", LocalDateTime.now());
        map.put("status", 0);
        map.put("points", points);
        map.put("level", level);
        map.put("popularity", 0);

        //update the challenge in cassandra
        if (!casDB.Update("challenge", map, "challengeid=" + challengeid)) {
            addUserError(getPersIDFromToken(token), "The challenge " + name + " could not be updated", null);
            return null;
        }

        map.put("id", uuid);
        map.put("postid", postid);
        map.put("fileid", fileid);

        //get the userid of the creator of the challenge
        map.put("createdbyuserid", casDB.Select("challenge", new String[]{"persid_user"}, "challengeid=" + challengeid, 0, null, null).one().getObject(0).toString());
        map.remove("challengeid");
        map.remove("persid_user");
        map.remove("file_fileid_file");
        map.remove("postid");

        l().info("The challenge " + name + " with the id " + uuid + " was successful updated");

        return map;
    }

    public Map<String, Object> deleteChallenge(String token, String challengeid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", challengeid);

        if (!challengeExists(challengeid)) {
            l().info("The challenge is already deleted");
            return map;
        }

        Map<String, Object> challange = getChallenges(challengeid, null).get(0);

        deletePost(token, challange.get("postid").toString());
        deleteFile(token, challange.get("fileid").toString());

        if (!casDB.Delete("challenge", "challengeid=" + challengeid)) {
            addUserError(getPersIDFromToken(token), "The challenge " + challengeid + " could not be deleted", null);
            return null;
        }

        neoDB.DeleteAsync("(e1:Challenge{id:'" + challengeid + "'})", true, "e1", null);

        l().info("The challenge with the id " + challengeid + " was successful deleted");

        return map;
    }

    public Map<String, Object> addCompletedChallenge(String token, String challid, String postid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
            return null;
        }

        if (!postExists(postid)) {
            addUserError(getPersIDFromToken(token), "The post doesn't exist", postid);
            return null;
        }

        if (!challengeExists(challid)) {
            addUserError(getPersIDFromToken(token), "The challenge doesn't exist", challid);
            return null;
        }

        String uuid = casDB.getRandomUUID();

        //create the connection for a completed challenge
        String create = "(e4:Challenge_completed{id:'" + uuid + "', timestamp:'" + LocalDateTime.now() + "'}),(e1)-[c1:has]->(e4),(e4)-[c2:has]->(e2),(e4)-[c3:has]->(e3)";
        if (!neoDB.Create("(e1:User{id:'" + getPersIDFromToken(token) + "'}),(e2:Challenge{id:'" + challid + "'}),(e3:Post{id:'" + postid + "'})", null, create, "e1", null)) {
            addUserError(getPersIDFromToken(token), "The completed challenge couldn't be added.", null);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", uuid);
        map.put("persid", getPersIDFromToken(token));
        map.put("postid", postid);
        map.put("challengeid", challid);

        l().info("The completed challenge with the id " + uuid + " was successful added");

        return map;
    }

    public List<Map<String, Object>> getCompletedChallenge(String token, String compChallid, String persid, String challid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
            return null;
        }

        if (challid != null && !challengeExists(challid)) {
            addUserError(getPersIDFromToken(token), "The challenge does not exist", challid);
            return null;
        }

        if (persid != null && !userExists(persid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", persid);
            return null;
        }

        if (compChallid != null && !completedChallegeExists(compChallid)) {
            addUserError(getPersIDFromToken(token), "The user does not exist", persid);
            return null;
        }

        List<Map<String, Object>> list;

        //get the completed challenge based on one of the input parameters
        if (compChallid != null) {
            list = neoDB.Match("(e1:Challenge_completed{id:'" + compChallid + "'}--[:has]--(other))", null, null, "e1.id, others", null, null, null);     //return other.id
        } else if (persid != null) {
            String match = "(e1:User{id:'" + persid + "'})-[:has]->(e2:Challenge_completed)-[:has]->(e3:Post)," +
                    "(e4:User{id:'" + persid + "'})-[:has]->(e5:Challenge_completed)-[:has]->(e6:challenge)";
            list = neoDB.Match(match, null, null, "e1.id AS persid, e2.id AS id, e3.id AS postid, e6.id AS challengeid", null, null, null);
        } else {
            String match = "(e1:Challenge{id:'" + challid + "'})-[:has]--(e2:Challenge_completed)-[:has]->(e3:Post)," +
                    "(e4:Challenge{id:'" + challid + "'})-[:has]->(e5:Challenge_completed)-[:has]->(e6:User)";
            list = neoDB.Match(match, null, null, "e1.id challengeid, e2.id AS id, e3.id AS postid, e6.id AS persid", null, null, null);
        }

        l().info("The completed challenges were successfully fetched.");

        return list;
    }

    public boolean completedChallegeExists(String compChallid) {
        return casDB.isUUID(compChallid) && neoDB.Match("(e1:Completed_challenge{'" + compChallid + "'})", null, null, "e1", null, null, null).size() > 1;
    }

    public Map<String, Object> deleteCompletedChallenge(String token, String compChallid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user with the token is not logged in", token);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", compChallid);

        if (!completedChallegeExists(compChallid)) {
            l().info("The completed challenge is already deleted");
            return map;
        }

        //a challenge includes a evidence post that he succeeded the challenge
        //this post gets deleted
        Map<String, Object> entry = getCompletedChallenge(token, null, null, compChallid).get(0);
        deletePost(token, entry.get("postid").toString());

        //delete the completion from neo4j
        if (!neoDB.Delete("(n:Challenge_completed{id:'" + compChallid + "'})  ", false, "n", null)) {
            addUserError(getPersIDFromToken(token), "The completed challenge " + compChallid + " could not be deleted", null);
            return null;
        }

        l().info("The completed challenge with the id " + compChallid + " was successful deleted");

        return map;
    }


}
