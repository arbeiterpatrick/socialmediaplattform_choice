
package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import org.slf4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Access {


    private E10_Modie highestlayer;

    private Access() {
        highestlayer = new E10_Modie();

    }

    public static Access getInstance() {
        return AccessHolder.INSTANCE;
    }

    private static class AccessHolder {

        private static final Access INSTANCE = new Access();
    }


    
    public Map<String, Object> addFile(String token, String name, String contend) {
        return highestlayer.addFile(token, name, contend);
    }

    
    public List<Map<String, Object>> getUserBackgroundimage(String token, String firstname, String surname) {
        return highestlayer.getUserBackgroundimage(token, firstname, surname);
    }

    
    public List<Map<String, Object>> getUserProfilepicture(String token, String firstname, String surname) {
        return highestlayer.getUserProfilepicture(token, firstname, surname);
    }

    
    public List<Map<String, Object>> getFile(String token, String id) {
        return highestlayer.getFile(token, id);
    }

    
    public void addUserAction(String user, String action, boolean permitted) {
        highestlayer.addUserAction(user, action, permitted);
    }

    
    public void addLoginLog(String uuid, String user) {
        highestlayer.addLoginLog(uuid, user);
    }

    
    public String getRandomUUID() {
        return highestlayer.getRandomUUID();
    }

    
    public void addAccessAttempt(String header, String query, String user) {
        highestlayer.addAccessAttempt(header, query, user);
    }

    
    public LinkedList<String> getUserPermissions(String token) {
        return highestlayer.getUserPermissions(token);
    }

    
    public String loginUser(String email, String password, String token) {
        return highestlayer.loginUser(email, password, token);
    }

    
    public boolean logoutUser(String email, String token) {
        return highestlayer.logoutUser(email, token);
    }

    
    public boolean accessAllowed(String token, String permission) {
        return highestlayer.accessAllowed(token, permission);
    }

    
    public Map<String, Object> getUserInfo(String token) {
        return highestlayer.getUserInfo(token);
    }

    
    public List<Map<String, Object>> getChallenges(String id, String name) {
        return highestlayer.getChallenges(id, name);
    }

    
    public Logger l() {
        return highestlayer.l();
    }

    
    public void setIP(String ip) {
        highestlayer.setIP(ip);
    }


    
    public boolean challengeExists(String id) {
        return highestlayer.challengeExists(id);
    }

    
    public Map<String, Object> addChallenge(String name, String teaser, int points, int level, String token, String fileid, String postid) {
        return highestlayer.addChallenge(name, teaser, points, level, token, fileid, postid);
    }

    
    public boolean postExists(String id) {
        return highestlayer.postExists(id);
    }

    
    public boolean fileExists(String id) {
        return highestlayer.fileExists(id);
    }

    
    public void addUserError(String user, String errormessage, String error_creating_value) {
        highestlayer.addUserError(user, errormessage, error_creating_value);
    }

    
    public Map<String, Object> editLoginCredentials(String token, String email, String persid, String newPassword) {
        return highestlayer.editLoginCredentials(token, email, persid, newPassword);
    }

    
    public boolean userExists(String persid) {
        return highestlayer.userExists(persid);
    }

    
    public boolean isEmail(String email) {
        return highestlayer.isEmail(email);
    }

    
    public boolean isLoggedIn(String token) {
        return highestlayer.isLoggedIn(token);
    }

    
    public String getPersIDFromToken(String token) {
        return highestlayer.getPersIDFromToken(token);
    }

    
    public Map<String, Object> editUser(String persid, String token, String firstname, String surname, String aboutme, String residence, String birthday, String employment, String fontface, String backgroundcolor, int fontsize) {
        return highestlayer.editUser(persid, token, firstname, surname, aboutme, residence, birthday, employment, fontface, backgroundcolor, fontsize);
    }

    
    public Map<String, Object> addUser(String email, String password, String firstname, String surname, String aboutme, String residence, String birthday, String employment, String fontface, String backgroundcolor, int fontsize) {
        return highestlayer.addUser(email, password, firstname, surname, aboutme, residence, birthday, employment, fontface, backgroundcolor, fontsize);
    }

    
    public Map<String, Object> deleteUser(String persid, String password, String email, String token) {
        return highestlayer.deleteUser(persid, password, email, token);
    }

    
    public List<Map<String, Object>> getUsers(String persid, String token, boolean current, String vorname, String nachname) {
        return highestlayer.getUsers(persid, token, current, vorname, nachname);
    }

    
    public List<Map<String, Object>> getUsersFriends(String persid, String token) {
        return highestlayer.getUsersFriends(persid, token);
    }

    
    public List<Map<String, Object>> getUserscompletedChallenge(String challid, String token) {
        return highestlayer.getUserscompletedChallenge(challid, token);
    }

    
    public Map<String, Object> addPost(String token, String message, String viewmode, String fileid) {
        return highestlayer.addPost(token, message, viewmode, fileid);
    }

    
    public Map<String, Object> editPost(String token, String postid, String message, String viewmode, String fileid) {
        return highestlayer.editPost(token, postid, message, viewmode, fileid);
    }

    
    public Map<String, Object> deletePost(String token, String postid) {
        return highestlayer.deletePost(token, postid);
    }

    
    public List<Map<String, Object>> getPosts(String token, String postid, String persid) {
        return highestlayer.getPosts(token, postid, persid);
    }

    
    public Map<String, Object> editChallenge(String name, String teaser, int points, int level, String token, String fileid, String postid, String challengeid) {
        return highestlayer.editChallenge(name, teaser, points, level, token, fileid, postid, challengeid);
    }

    
    public Map<String, Object> deleteChallenge(String token, String challengeid) {
        return highestlayer.deleteChallenge(token, challengeid);
    }

    
    public Map<String, Object> deleteFile(String token, String id) {
        return highestlayer.deleteFile(token, id);
    }

    
    public boolean commentExists(String commentid) {
        return highestlayer.commentExists(commentid);
    }

    
    public List<Map<String, Object>> getComments(String token, String postid, String persid, String commentid) {
        return highestlayer.getComments(token, postid, persid, commentid);
    }

    
    public Map<String, Object> addComment(String token, String message, String postid) {
        return highestlayer.addComment(token, message, postid);
    }

    
    public Map<String, Object> editComment(String token, String commentid, String message) {
        return highestlayer.editComment(token, commentid, message);
    }

    
    public Map<String, Object> deleteComment(String token, String commentid) {
        return highestlayer.deleteComment(token, commentid);
    }

    
    public Map<String, Object> addCompletedChallenge(String token, String challid, String postid) {
        return highestlayer.addCompletedChallenge(token, challid, postid);
    }

    
    public List<Map<String, Object>> getCompletedChallenge(String token, String compChallid, String persid, String challid) {
        return highestlayer.getCompletedChallenge(token, compChallid, persid, challid);
    }

    
    public boolean completedChallegeExists(String compChallid) {
        return highestlayer.completedChallegeExists(compChallid);
    }

    
    public Map<String, Object> deleteCompletedChallenge(String token, String compChallid) {
        return highestlayer.deleteCompletedChallenge(token, compChallid);
    }

    
    public List<Map<String, Object>> getFriends(String token, String persid) {
        return highestlayer.getFriends(token, persid);
    }

    
    public Map<String, Object> addFriend(String token, String persid, String type) {
        return highestlayer.addFriend(token, persid, type);
    }

    
    public Map<String, Object> deleteFriend(String token, String persid) {
        return highestlayer.deleteFriend(token, persid);
    }

    
    public List<Map<String, Object>> getLikes(String token, String id, String type) {
        return highestlayer.getLikes(token, id, type);
    }

    
    public Map<String, Object> addLike(String token, String chind, String id, String type) {
        return highestlayer.addLike(token, chind, id, type);
    }

    
    public Map<String, Object> deleteLike(String token, String id) {
        return highestlayer.deleteLike(token, id);
    }

    
    public List<Map<String, Object>> getChat(String token, String id, String persid) {
        return highestlayer.getChat(token, id, persid);
    }

    
    public Map<String, Object> deleteChat(String token, String id) {
        return highestlayer.deleteChat(token, id);
    }

    
    public List<Map<String, Object>> getMessages(String token, String id, String chatid) {
        return highestlayer.getMessages(token, id, chatid);
    }

    
    public Map<String, Object> addMessage(String token, String message, String persid, String fileid) {
        return highestlayer.addMessage(token, message, persid, fileid);
    }

    
    public Map<String, Object> deleteMessage(String token, String id) {
        return highestlayer.deleteMessage(token, id);
    }

    
    public Map<String, Object> editMessage(String token, String message, String id) {
        return highestlayer.editMessage(token, message, id);
    }

    
    public Map<String, Object> setTheme(String token, String name, String fontsize, String fontface, String backgroundcolor, String fileid) {
        return highestlayer.setTheme(token, name, fontsize, fontface, backgroundcolor, fileid);
    }

    
    public List<Map<String, Object>> getTheme(String token) {
        return highestlayer.getTheme(token);
    }

    
    public Map<String, Object> setModie(String token, String name) {
        return highestlayer.setModie(token, name);
    }

    
    public List<Map<String, Object>> getModie(String token) {
        return highestlayer.getModie(token);
    }

}
