
package project.BL.Encryption;

import project.Util.Util;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
public class EncryptionAccess {

    public static void main(String[] args) {
        EncryptionAccess t = new EncryptionAccess();
        t.test("Very secret text");
    }

    public EncryptionAccess() {
        encryption_method = Util.encryption_method;
        init();
    }

    public void test(String text) {
        System.out.println("SRC: https://blog.axxg.de/java-verschluesselung-beispiel-quickstart/");
        System.out.println("SRC2: http://blog.axxg.de/java-aes-rsa-key-file/");
        System.out.println("Es wird bei RSA davon ausgegangen, dass hier mit dem Privatekey entschlüsselt wird.");
        System.out.println("------------------------");

        System.out.println("Crypt test");
        System.out.println("-----------------------------");
        System.out.println("Text that is worked with: " + text);
        System.out.println("");

        //AES
        System.out.println("Crypt with AES");
        setEncryption_Method("AES");

        String enc = encrypt(text);
        System.out.println("Encrypted text: " + enc);

        String dec = decrypt(enc);
        System.out.println("Dectypted text: " + dec);

        //RSA
        System.out.println("Crypt with RSA");
        setEncryption_Method("RSA");

        enc = encrypt(text);
        System.out.println("Encrypted text: " + enc);

        dec = decrypt(enc);
        System.out.println("Dectypted text: " + dec);
    }

    private SecretKey key_aes;
    private PublicKey key_rsa_public;
    private PrivateKey key_rsa_private;
    private EasyCrypt ec;
    private EasyCrypt ec_private;   //if RSA, it holts the private key
    private String encryption_method;

    public void setEncryption_Method(String encryption_method) {
        if (Util.allow_encryption_method_runntime_changes) {
            this.encryption_method = encryption_method;
            init();
        }
    }

    public String encrypt(String text) {
        //AES
        if (encryption_method.equals("AES")) {
            try {
                return ec.encrypt(text);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            //RSA
        } else if (encryption_method.equals("RSA")) {
            try {
                return ec.encrypt(text);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            //If unknown
        } else {
            try {
                throw new Exception("Wrong encyptionmethod " + encryption_method);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String decrypt(String text) {
        //AES
        if (encryption_method.equals("AES")) {
            try {
                return ec.decrypt(text);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            //RSA
        } else if (encryption_method.equals("RSA")) {
            try {
                return ec_private.decrypt(text);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            //If unknown
        } else {
            try {
                throw new Exception("Wrong encyptionmethod " + encryption_method);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    private void init() {
        if (encryption_method.equals("AES")) {

            //check if AES file exists and write key info file
            File f = new File(Util.filepath_AES);
            if (!(f.exists() && !f.isDirectory())) {        //if file doesn't exist
                writeAESKeytoFile(Util.filepath_AES);
            }

            //read AES Key
            key_aes = readAESKeyfromFile(Util.filepath_AES);

            //generate encryyption instance with the key
            try {
                ec = new EasyCrypt(key_aes, "AES");
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (encryption_method.equals("RSA")) {

            //check if RSA files exist and write keys into files
            File f = new File(Util.filepath_RSA_public);
            File f2 = new File(Util.filepath_RSA_private);
            if ((!(f.exists() && !f.isDirectory())) || !(f2.exists() && !f2.isDirectory())) {        //if files doesn't exist
                writeRSAKeytoFiles(Util.filepath_RSA_private, Util.filepath_RSA_public);
            }

            //read keys
            key_rsa_public = readPublicRSAKeyfromFile(Util.filepath_RSA_public);
            key_rsa_private = readPrivateRSAKeyfromFile(Util.filepath_RSA_private);

            //generate encryyption instance with the key
            try {
                ec = new EasyCrypt(key_rsa_public, "RSA");
                ec_private = new EasyCrypt(key_rsa_private, "RSA");
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            //if unknown
            try {
                throw new Exception("Wrong encyptionmethod " + encryption_method);
            } catch (Exception ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void writeRSAKeytoFiles(String private_path, String public_path) {

        try {
            // generate random key
            KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
            keygen.initialize(1024);
            KeyPair keyPair = keygen.genKeyPair();

            // read key
            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();

            // save Public Key
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
            FileOutputStream fos = new FileOutputStream(public_path);
            fos.write(x509EncodedKeySpec.getEncoded());
            fos.close();

            //save Private Key
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
            fos = new FileOutputStream(private_path);
            fos.write(pkcs8EncodedKeySpec.getEncoded());
            fos.close();
        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void writeAESKeytoFile(String path) {
        try {
            // generate random keys
            KeyGenerator keygen = KeyGenerator.getInstance("AES");
            keygen.init(128);

            //write key into file
            byte[] bytes = keygen.generateKey().getEncoded();
            FileOutputStream keyfos = new FileOutputStream(new File(path));
            keyfos.write(bytes);
            keyfos.close();
        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PrivateKey readPrivateRSAKeyfromFile(String path) {
        FileInputStream fis = null;
        try {
            File dateiPriK = new File(path);
            //read private key
            fis = new FileInputStream(dateiPriK);
            byte[] encodedPrivateKey = new byte[(int) dateiPriK.length()];
            fis.read(encodedPrivateKey);
            fis.close();

            // generate Key
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    private PublicKey readPublicRSAKeyfromFile(String path) {
        FileInputStream fis = null;
        try {
            File dateiPubK = new File(path);

            //read punlic key
            fis = new FileInputStream(dateiPubK);
            byte[] encodedPublicKey = new byte[(int) dateiPubK.length()];
            fis.read(encodedPublicKey);
            fis.close();

            // generate Key
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
            return keyFactory.generatePublic(publicKeySpec);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    private SecretKey readAESKeyfromFile(String path) {
        try {
            File datei = new File(path);

            //read key
            FileInputStream fis = new FileInputStream(datei);
            byte[] encodedKey = new byte[(int) datei.length()];
            fis.read(encodedKey);
            fis.close();

            //generate key
            return new SecretKeySpec(encodedKey, "AES");
        } catch (IOException ex) {
            Logger.getLogger(EncryptionAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
