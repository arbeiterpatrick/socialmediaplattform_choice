package project.BL;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.datastax.driver.core.Row;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class E9_Theme extends E8_Chat {
    //This class has no special functionality, it just supports the frontend, where the real magic is happening

    public Map<String, Object> setTheme(String token, String name, String fontsize, String fontface, String backgroundcolor, String fileid) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not loggedin", token);
            return null;
        }

        if (fileid != null && !fileExists(fileid)) {
            addUserError(getPersIDFromToken(token), "The fileid is not valid", fileid);
            return null;
        }

        int i_fontsize = -1;

        //convert and check the fontsize
        try {
            i_fontsize = Integer.parseInt(fontsize);
            if (i_fontsize < 0) {
                throw new Exception();
            }

        } catch (Exception ex) {
            addUserError(getPersIDFromToken(token), "The fontsize is not valid.", fontsize);
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("fontsize", i_fontsize);
        map.put("backgroundimage_fileid_file", fileid);
        map.put("theme_name", name);
        map.put("backgroundcolor", backgroundcolor);
        map.put("fontface", fontface);

        //update the theme of a user
        if (!casDB.Update("user", map, "persid=" + getPersIDFromToken(token))) {
            addUserError(getPersIDFromToken(token), "Could not set the theme", null);
            return null;
        }

        l().info("Setting the theme " + name + " was successful");

        return getTheme(token).get(0);

    }

    public List<Map<String, Object>> getTheme(String token) {
        if (!isLoggedIn(token)) {
            addUserError(null, "The user is not logged in", token);
            return null;
        }

        //get the theme
        Row r = casDB.Select("post", new String[]{"theme_name", "backgroundimage_fileid_file", "fontface", "backgroundcolor", "fontsize"}, "persid=" + getPersIDFromToken(token), 0, null, null).one();

        String[] calls = new String[]{"name", "fileid", "fontface", "backgroundcolor"};

        List<Map<String, Object>> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();

        //rename the keys
        for (int i = 0; i < calls.length; i++) {
            map.put(calls[i], r.getObject(i).toString());
        }

        map.put("fontsize", r.getInt("fontsize"));

        list.add(map);

        l().info("The theme were successful fetched from the database.");

        return list;

    }


}
