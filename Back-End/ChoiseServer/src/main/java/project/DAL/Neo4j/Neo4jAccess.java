
package project.DAL.Neo4j;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.neo4j.driver.v1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.Util.Util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
public class Neo4jAccess {
    private Logger l;

    private Neo4jAccess() {
        l = LoggerFactory.getLogger(this.getClass().getName());
        DBConnect();
    }

    public static void main(String[] args) {
        //only for testing
        Neo4jAccess n = Neo4jAccess.getInstance();
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        System.out.println("Create element");
        System.out.println(gson.toJson(n.Query("CREATE (f1:Test{name:'r1'}) return count(f1) as count", null)));
        System.out.println("Create Relationship");
        System.out.println(gson.toJson(n.Query("MATCH (s:Test{name:'r1'}), (x:Example) CREATE  (s)-[r:testing]->(x) return count(r) as count", null)));
        System.out.println("Delete");
        System.out.println(gson.toJson(n.Query("MATCH (s:Test{name:'r1'}) DELETE s return count(s) as count", null)));

    }

    public static Neo4jAccess getInstance() {
        return Neo4jAccessHolder.INSTANCE;
    }

    private static class Neo4jAccessHolder {

        private static final Neo4jAccess INSTANCE = new Neo4jAccess();
    }

    private Driver driver;

    private void DBConnect() {
        String uri = Util.NEO4J_URI;

        //connect to the database
        try {
            String auth = new URL(uri.replace("bolt", "http")).getUserInfo();
            if (auth != null) {
                String[] parts = auth.split(":");

                driver = GraphDatabase.driver(uri, AuthTokens.basic(parts[0], parts[1]), Config.build().toConfig());
            } else {
                driver = GraphDatabase.driver(uri, AuthTokens.none(), Config.build().toConfig());
            }
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Invalid Neo4j-ServerURL " + uri);
        }
    }

    static Object convert(Value value) {
        switch (value.type().name()) {
            case "PATH":
                return value.asList(Neo4jAccess::convert);
            case "NODE":
            case "RELATIONSHIP":
                return value.asMap();
        }
        return value.asObject();
    }

    public Iterator<Map<String, Object>> Query(String query, Map<String, Object> params) {
        try (Session session = driver.session()) {
            l.info("NEO STATEMENT: " + query);

            //execute
            List<Map<String, Object>> list = session.run(query, params).list(r -> r.asMap(Neo4jAccess::convert));
            return list.iterator();
        }
    }

    /**
     * @param query  The returntype has to be a "count" that is named "count".If
     *               a variable should be inserted later, it can be let free and insert simply
     *               insert "{name_of_variable}". The variable can then be assigned via the
     *               params parameter
     * @param params
     */
    public void TransactionAsync(String query, Map<String, Object> params) {
        try (Session session = driver.session()) {
            l.info("STATEMENT: " + query);

            //execute
            session.runAsync(query, params).thenCompose(cursor -> cursor.forEachAsync(x -> {
                l.info(x.get("count").isNull() ? "" : ("Affected elements: " + x.get("count")));
            })).whenComplete((record, error) -> {
                //when done check if successful
                if (error != null) {
                    error.printStackTrace();
                }
                session.closeAsync();
            }).exceptionally((x) -> {
                x.printStackTrace();
                return null;
            });
        }
    }

    /**
     * @param query  The returntype has to be a "count" that is named "count". If
     *               a variable should be inserted later, it can be let free and insert simply
     *               insert "{name_of_variable}". The variable can then be assigned via the
     *               params parameter
     * @param params
     * @return
     */
    public int Transaction(String query, Map<String, Object> params) {
        try (Session session = driver.session()) {
            l.info("NEO STATEMENT: " + query);

            //execute
            String count = session.run(query, params).list(r -> r.asMap(Neo4jAccess::convert)).get(0).get("count").toString();
            l.info("Affected elements: " + count);

            //return affected elements
            return Integer.parseInt(count);
        } catch (Exception e) {
            l.error(e.getMessage(), e.getCause());
            return -1;
        }
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param where        can be empty
     * @param create
     * @param returnobject value can be renamed with " AS " <new_name>
     * @param params       can be empty
     * @return
     */
    public boolean Create(String match, String where, String create, String returnobject, Map<String, Object> params) {
        where = (where == null || where.isEmpty()) ? "" : " WHERE " + where;
        match = (match == null || match.isEmpty()) ? "" : " MATCH " + match;

        //execute transaction
        try {
            return Transaction(match + where + " CREATE " + create + " RETURN COUNT(" + returnobject + ") AS count", params) > 0;
        } catch (Exception e) {
            l.error(e.getMessage(), e.getCause());
            return false;
        }
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param where        can be empty
     * @param create
     * @param returnobject value can be renamed with " AS " <new_name>
     * @param params       can be empty
     */
    public void CreateAsync(String match, String where, String create, String returnobject, Map<String, Object> params) {
        where = (where == null || where.isEmpty()) ? "" : " WHERE " + where;
        match = (match == null || match.isEmpty()) ? "" : " MATCH " + match;

        //execute transaction
        try {
            TransactionAsync(match + where + " CREATE " + create + " RETURN COUNT(" + returnobject + ") AS count", params);
        } catch (Exception e) {
            l.error(e.getMessage(), e.getCause());
        }
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param delete_variable value can be renamed with " AS " <new_name>
     * @param params          can be empty
     * @return
     */
    public boolean Delete(String match, boolean detach, String delete_variable, Map<String, Object> params) {
        return Transaction("MATCH " + match + (detach ? " DETACH" : "") + " DELETE " + delete_variable, params) > 0;
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param returnobject value can be renamed with " AS " <new_name>
     * @param params       can be empty
     */
    public void DeleteAsync(String match, boolean detach, String delete_variable, Map<String, Object> params) {
        TransactionAsync("MATCH " + match + (detach ? " DETACH" : "") + " DELETE " + delete_variable, params);
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param set
     * @param returnobject value can be renamed with " AS " <new_name>
     * @param params       can be empty
     * @return
     */
    public boolean Update(String match, String set, String returnobject, Map<String, Object> params) {
        return Transaction("MATCH " + match + " SET " + set + " RETURN COUNT(" + returnobject + ") AS count", params) > 0;
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param set
     * @param returnobject value can be renamed with " AS " <new_name>
     * @param params       can be empty
     */
    public void UpdateAsync(String match, String set, String returnobject, Map<String, Object> params) {
        TransactionAsync("MATCH " + match + " SET " + set + " RETURN COUNT(" + returnobject + ") AS count", params);
    }

    public List<Map<String, Object>> Match(String match, String where, String unwind, String returnx) {
        return Match(match, where, unwind, returnx, null, null, null);
    }

    /**
     * If a variable should be inserted later, it can be let free and insert
     * simply insert "{name_of_variable}". The variable can then be assigned via
     * the params parameter
     *
     * @param match
     * @param where   can be empty
     * @param unwind  can be empty
     * @param returnx
     * @param orderby can be empty
     * @param limit   can be empty
     * @param params  can be empty
     * @return
     */
    public List<Map<String, Object>> Match(String match, String where, String unwind, String returnx, String orderby, String limit, Map<String, Object> params) {
        where = where == null || where.isEmpty() ? "" : " WHERE " + where;
        unwind = unwind == null || unwind.isEmpty() ? "" : " UNWIND " + unwind;
        limit = limit == null || limit.isEmpty() ? "" : " LIMIT " + limit;
        orderby = orderby == null || orderby.isEmpty() ? "" : " ORDER BY " + orderby;

        //thow error if no return variable is set
        if (returnx == null) {
            try {
                throw new Exception("The returnvalue can't be null");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //execute
        Iterator<Map<String, Object>> rows = Query("MATCH " + match + where + unwind + " RETURN " + returnx + orderby + limit, params);
        List<Map<String, Object>> list = new LinkedList<>();

        //convert result in a list of maps
        while (rows.hasNext()) {
            list.add(rows.next());
        }

        return list;
    }

}
