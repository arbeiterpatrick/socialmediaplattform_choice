-- DROP KEYSPACE mykeyspace;

-- CREATE KEYSPACE mykeyspace WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };

--Each comment has to end with a semicolon;
--Otherwise the following sql statement will not be executed;

--/**
-- * @author Manuel Weixler
-- * @email manuel@weixler.me
-- * @copyright Manuel Weixler
-- */


CREATE TABLE IF NOT EXISTS mykeyspace.user
  (
     persid                      UUID,
     firstname                   TEXT,
     surname                     TEXT,
     email                       TEXT,
     password                    TEXT,
     permission_id               INT,
     challenge_points            INT,
     permission_name             TEXT,
     about_me                    TEXT,
     residence                   TEXT,
     birthday                    TIMESTAMP,
     employment                  TEXT,
     profilepicture_fileid_file  INT,
     theme_name                  TEXT,
     backgroundimage_fileid_file INT,
     fontface                    TEXT,
     backgroundcolor             TEXT,
     fontsize                    INT,
     modie_id                    INT,
     dataset_state               INT,
     modie_name                  TEXT,
     session_token               UUID,
     session_timestamp           TIMESTAMP,
  PRIMARY KEY(persid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.user(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.file
  (
     fileid           UUID,
     name             TEXT,
     type             TEXT,
     createdtimestamp        TIMESTAMP,
     dataset_state    INT,
     size             INT,
     contend          TEXT,
       PRIMARY KEY(fileid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.file(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.post
  (
     postid           UUID,
     persid_user      UUID,
     message          TEXT,
     fileid_file      UUID,
     createdtimestamp TIMESTAMP,
     changedtimestamp TIMESTAMP,
     dataset_state    INT,
     viewmode         INT,
  PRIMARY KEY(postid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.post(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.comment
  (
     commentid               UUID,
     postid_post             UUID,
     persid_user             UUID,
     createdTimestamp        TIMESTAMP,
     editedTimestamp         TIMESTAMP,
     message                 TEXT,
     dataset_state           INT,
     PRIMARY KEY(persid_user, postid_post,dataset_state)
  ) WITH CLUSTERING ORDER BY (postid_post DESC);

CREATE INDEX IF NOT EXISTS ON mykeyspace.comment(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.friends
  (
     persid1_user                UUID,
     persid2_user                UUID,
     relationshipid_relationship UUID,
     relationshiptype            INT,
     dataset_state               INT,
     PRIMARY KEY(persid1_user, persid2_user)
  ) WITH CLUSTERING ORDER BY (persid2_user DESC);

CREATE INDEX IF NOT EXISTS ON mykeyspace.friends(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.relationship
  (
     relationshipid              UUID,
     dataset_state               INT,
     relationshiptype            INT,
  PRIMARY KEY(relationshipid)
);

CREATE TABLE IF NOT EXISTS mykeyspace.usererrorlog
  (
     logid                       UUID,
     loggedin_user               TEXT,
     added                       TIMESTAMP,
     error                       TEXT,
     dataset_state               INT,
     error_creating_value        TEXT,
  PRIMARY KEY(logid)
);
CREATE TABLE IF NOT EXISTS mykeyspace.useractionlog
  (
     logid                       UUID,
     loggedin_user               TEXT,
     added                       TIMESTAMP,
     action                      TEXT,
     dataset_state               INT,
     permitted                   TEXT,
  PRIMARY KEY(logid)
);

CREATE TABLE IF NOT EXISTS mykeyspace.loginlog
  (
     usertoken                   UUID,
     persid                      UUID,
     added                       TIMESTAMP,
     ip                          TEXT,
  dataset_state               INT,
  PRIMARY KEY(usertoken, added,dataset_state)
  );

CREATE TABLE IF NOT EXISTS mykeyspace.accesslog
  (
     logid                       UUID,
     access_header               TEXT,
     access_query                TEXT,
     encrypted                   TEXT,
     dataset_state               INT,
     loggedin_user               TEXT,
     added                       TIMESTAMP,
  PRIMARY KEY(logid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.relationship(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.modie
  (
     modieid                     UUID,
     dataset_state               INT,
     modiename                   TEXT,
  PRIMARY KEY(modieid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.modie(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.kind
  (
     kindid                  UUID,
     dataset_state           INT,
     kindname                TEXT,
  PRIMARY KEY(kindid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.kind(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.chat
  (
     chatid                  UUID,
     persid1_user            UUID,
     persid2_user            UUID,
     unreadmessages          INT,
     messagepreview          TEXT,
     received_timestamp      TIMESTAMP,
     dataset_state           INT,
     PRIMARY KEY(chatid)
  );

CREATE TABLE IF NOT EXISTS mykeyspace.message
  (
     chatid                  UUID,
     messageid               UUID,
     message                 TEXT,
     createdtimestamp        TIMESTAMP,
     fileid                  UUID,
     editedtimestamp         TIMESTAMP,
     dataset_state           INT,
     PRIMARY KEY(chatid)
  );

CREATE INDEX IF NOT EXISTS ON mykeyspace.chat(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.permission
  (
     permissionid            UUID,
     dataset_state           INT,
     permissionname          TEXT,
  PRIMARY KEY(permissionid)

);

CREATE INDEX IF NOT EXISTS ON mykeyspace.permission(dataset_state);

CREATE TABLE IF NOT EXISTS mykeyspace.challenge
  (
     challengeid      UUID,
     persid_user      UUID,
     name             TEXT,
     teaser           TEXT,
     file_fileid_file UUID,
     postid           UUID,
     createdtimestamp TIMESTAMP,
     dataset_state    INT,
     status           INT,
     points           INT,
     level            INT,
     popularity       INT,
  PRIMARY KEY(challengeid)
);

CREATE INDEX IF NOT EXISTS ON mykeyspace.challenge(dataset_state);


