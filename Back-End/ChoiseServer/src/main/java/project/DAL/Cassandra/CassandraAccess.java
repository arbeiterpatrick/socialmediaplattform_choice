package project.DAL.Cassandra;

import com.datastax.driver.core.*;
import com.datastax.driver.core.utils.UUIDs;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.Util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
public class CassandraAccess {

    private static Logger l;
    private static Session session = null;
    protected static Cluster cluster = null;
    protected static CassandraAccess instance = null;

    @Override
    protected void finalize() {
        //close everything
        try {
            super.finalize();
            session.close();
            cluster.close();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    //path to the DDL file
    private static String path;

    //for test_CRUD
    public static void main(String[] args) {
        CassandraAccess.getInstance().commandBoard();
    }

    //Singleton
    protected CassandraAccess() {
        l = LoggerFactory.getLogger(this.getClass().getName());
        //set path to the DDL file
        path = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "java"
                + File.separator + "project" + File.separator + "DAL" + File.separator + "Cassandra" + File.separator + "schema_v2.sql";
    }

    public static CassandraAccess getInstance() {
        if (instance == null || cluster == null || session == null) {
            instance = new CassandraAccess();
            //write db infos into terminal
            instance.dbInformation();

            //build cluster instance
            if (Util.user == null || Util.user.trim().isEmpty() || Util.password == null || Util.password.trim().isEmpty()) {
                cluster = Cluster.builder().addContactPoint(Util.clusternode).
                        withPort(Util.port).
                        withoutMetrics().
                        build();
            } else {
                if (Util.useSSL) {
                    cluster = Cluster.builder().addContactPoint(Util.clusternode).withPort(Util.port).withCredentials(Util.user.trim(), Util.password.trim()).withSSL().withoutMetrics().build();
                } else {
                    cluster = Cluster.builder().addContactPoint(Util.clusternode).withPort(Util.port).withCredentials(Util.user.trim(), Util.password.trim()).withoutMetrics().build();
                }
            }
            //create a session
            try {
                session = cluster.connect(Util.keyspace);
            } catch (Exception ex) {

                //if keyspace does not exist create it
                if (createKeyspace(cluster)) {
                    session = cluster.connect(Util.keyspace);
                    l.info("The keyspace \"" + Util.keyspace + "\" has been created.");
                } else {
                    l.error(ex.getMessage(), ex.getCause());
                }

            }

            //create missing tables
            if (createMissingTables(false)) {
                l.info("All missing tables and usertypes have been created.");
            } else {
                //drop if mission tables couldn't be added
                if (Util.drop_tables_if_too_many) {
                    dropKeyspace();
                    try {
                        session = cluster.connect(Util.keyspace);
                    } catch (Exception ex) {
                        l.error(ex.getMessage(), ex.getCause());
                        System.exit(1);
                    }
                }

                if (createMissingTables(true)) {
                    l.info("All missing tables have been created.");
                }
            }
        }
        return instance;
    }

    private void dbInformation() {
        l.info("Beim erstellen des Clusterobjektes gibt es aktuell einen Bug der vom Hersteller der DB Treiber noch nicht behoben wurde.");
        l.info("Dieser beeinflusst das Programm jedoch nicht");
        l.info("SRC: https://docs.datastax.com/en/developer/java-driver/3.1/faq/osgi/");
        l.info("SRC: https://datastax-oss.atlassian.net/browse/JAVA-1127");

        l.info("Die Doku für das Cassandra Query Language findet man unter:");
        l.info("https://cassandra.apache.org/doc/cql3/CQL-3.0.html");
    }

    private static boolean createKeyspace(Cluster cluster) {
        try {
            if (Util.replication_factor < 1) {
                throw new Exception("The replication factor of the keyspace " + Util.keyspace + " needs to be larger then 0");
            }

            //create a session and keyspace
            Session s = cluster.newSession();
            String query = new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ")
                    .append(Util.keyspace).append(" WITH replication = {")
                    .append("'class':'").append(Util.replication_strategy)
                    .append("','replication_factor':").append(Util.replication_factor)
                    .append("};").toString();

            s.execute(query);
            s.close();
            return true;
        } catch (Exception ex) {
            l.error(ex.getMessage(), ex.getCause());
            return false;
        }
    }

    private static void dropKeyspace() {
        try {
            session.close();
            session = cluster.newSession();
            session.execute("DROP KEYSPACE " + Util.keyspace + ";");
            l.info("The keyspace " + Util.keyspace + " was sucessfull dropped");
            if (!createKeyspace(cluster)) {
                l.warn("The keyspace couldn't be created");
            }
        } catch (Exception ex) {
            l.error(ex.getMessage(), ex.getCause());
            System.exit(1);
        }
    }

    private static List<String> getCreateDDLStatements() {
        //get the DDL model

        try {

            //read file
            File file = new File(path);
            byte[] data;
            try (FileInputStream fis = new FileInputStream(file)) {
                data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
            }

            //list of characters that are no part of an "Create table" sql statement
            String[] items = {"-", ""};

            //prepare sql statements
            //1. remove all unseen characters except space
            //2. replace the default keyspacename
            //3. remove unnecessary spaces
            //4. split into statements
            //5. trim statements
            //6. remove statements that are no CREATE TABLE Statements
            return Arrays.asList(new String(data, "UTF-8").
                    replaceAll("[^A-Za-z0-9 ;()<,>._-]", "").
                    replaceAll("^ +| +$|( )+", "$1").
                    replaceAll("mykeyspace", Util.keyspace).split(";")).
                    stream().map(s -> s.trim()).
                    filter(f -> Arrays.stream(items).anyMatch(i -> !f.contains(i)))
                    .collect(Collectors.toList());
        } catch (FileNotFoundException ex) {
            l.error(ex.getMessage(), ex.getCause());
            System.exit(1);

            return null;
        } catch (IOException ex) {
            l.error(ex.getMessage(), ex.getCause());
            System.exit(1);

            return null;

        }
    }

    private static boolean createMissingTables(boolean ignore_table_check) {

        //get all table and userType names from the database
        List<String> tableNames = cluster.getMetadata().getKeyspace(Util.keyspace).getTables().stream().map(tm -> tm.getName().trim()).collect(Collectors.toList());
        List<String> userTypeNames = cluster.getMetadata().getKeyspace(Util.keyspace).getUserTypes().stream().map(tm -> tm.getTypeName().trim()).collect(Collectors.toList());

        //get all create table statements
        List<String> statements = getCreateDDLStatements();

        //check there are more tables in the database then in the DDL
        if ((!ignore_table_check) && statements.size() < tableNames.size() + userTypeNames.size()) {
            return false;
        }

        //print information about the table und usertypes onto terminal
        l.info("The DDL consists out of " + statements.size() + " statements(usertypes and tables)");
        l.info("The following tables(" + tableNames.size() + ") are already in the database (the others will be created):");
        if (tableNames.isEmpty()) {
            l.info("-No tables were found");
        } else {
            tableNames.forEach(l::info);
        }

        l.info("The following usertypes(" + userTypeNames.size() + ") are already in the database (the others will be created):");
        if (userTypeNames.isEmpty()) {
            l.info("-No usertypes were found");
        } else {
            userTypeNames.forEach(l::info);
        }

        //create all tables and usertypes
        try {
            for (String s : statements) {

                String element = s.replaceAll("CREATE TABLE IF NOT EXISTS " + Util.keyspace + ".", "").split("\\(")[0].trim();   //get table name from sql statement

                if (element.contains(" ")) {        //element is a usertype
                    element = element.replaceAll("CREATE TYPE IF NOT EXISTS " + Util.keyspace + ".", "").trim();   //get usertype name from sql statement
                    if (!userTypeNames.contains(element)) {
                        l.info("Create usertype: " + element);
                        l.info("STATEMENT: " + s);
                        session.execute(s + ";");
                    }
                } else if (!tableNames.contains(element)) {
                    l.info("Create table: " + element);
                    l.info("STATEMENT: " + s);
                    session.execute(s + ";");
                }
            }

            return true;
        } catch (Exception ex) {
            l.error(ex.getMessage(), ex.getCause());
            System.exit(1);

            return false;
        }

    }

    //this void is only for testing purposes
    private void commandBoard() {

        String statement = "";

        System.out.println("Input something:");
        statement = new Scanner(System.in).nextLine();

        System.out.println("COMMAND: " + statement);
        while (!statement.equals("exit")) {
            statement = statement.replaceAll("^ +| +$|( )+", "$1");
            try {
                ResultSet results = null;
                if (statement.equals("create tables")) {
                    if (createMissingTables(true)) {
                        System.out.println("All tables have been sucessfull created");
                    }

                } else if (statement.equals("playground")) {
                    this.test_CRUD();

                } else {
                    results = session.execute(statement);
                    for (Row row : results) {
                        System.out.println("EL X: " + row);
                    }
                }
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
            }

            System.out.println("Input something:");
            statement = new Scanner(System.in).nextLine();
            System.out.println("COMMAND: " + statement);
        }
        System.exit(0);
    }

    protected void test_CRUD() {
        Map<String, Object> data = new HashMap<>();
        String uuid = getRandomUUID();
        data.put("modieid", uuid);
        data.put("dataset_state", 0);
        data.put("modiename", "Test Modie");
        if (Insert("modie", data)) {
            System.out.println("++++++insert worked");
        } else {
            System.out.println("++++++insert doesn't work ");
        }

        //update start
        data.remove("modieid");
        data.replace("dataset_state", 3);
        data.replace("modiename", "Test2 mit dem Modiename");

        if (Update("modie", data, "modieid=" + uuid)) {
            System.out.println("++++++update worked");
        } else {
            System.out.println("++++++update doesn't work");
        }

        //start Select
        ResultSet rs = Select("modie", new String[]{"modiename", "modieid"}, "dataset_state=3", 0, null, null);
        if (rs != null) {
            System.out.println("++++++select worked");
        } else {
            System.out.println("++++++select doesn't work ");
        }

        rs.all().forEach(System.out::println);

        //start delete
        if (Delete("modie", "modieid=" + uuid)) {
            System.out.println("++++++deleten worked");
        } else {
            System.out.println("++++++delete doesn't work");
        }

        System.exit(0);
    }

    /**
     * Select data from the database
     * <p>
     * If the parameter is not needed use the value NULL or for int use 0
     *
     * @param table             tablename
     * @param columns           also allowed are the functions MIN, MAX, AVG, SUM and
     *                          COUNT, TODATE
     * @param where             the following operators are allowed: =, IN, =>, <=, <, >,
     *                          AND
     * @param limit             to get all write 0 (max is 10000)
     * @param uuid_last_dataset defines from which row on the data should be
     *                          queried. Needs to be the last uuid of the last query
     * @param orderBy
     * @return
     */
    public ResultSet Select(String table, String[] columns, String where, int limit, String uuid_last_dataset, String orderBy) {

        //check if where contains a wrong formatted uuid
        //build where clause
        if (where != null && !where.isEmpty()) {
            if (UUIDwrongFormated(where)) {
                try {
                    throw new Exception("The UUID is not allowed to be formatted like a string. Remove the singlequotes around it.");
                } catch (Exception ex) {
                    l.error(ex.getMessage(), ex.getCause());
                }
                return null;
            }
            where = " WHERE " + where;
        } else {
            where = "";
        }

        //build order by clause
        if (orderBy != null) {
            orderBy = " ORDER BY (" + orderBy + ")";
        } else {
            orderBy = "";
        }

        //build limit clause
        String endlimit = "";

        if (limit > 0) {
            endlimit = " LIMIT " + limit;
        }

        //create finished sql statement and logs it on the terminal
        String statement = "SELECT " + String.join(", ", columns) + " FROM " + Util.keyspace + "." + table + where + orderBy + endlimit + " ALLOW FILTERING";
        l.info("STATEMENT: " + statement);

        //execute statement
        ResultSet rs = session.execute(statement);

        //check if it was successfull
        return rs.wasApplied() ? rs : null;

    }

    //get a unique Random uuid from the databse
    public String getRandomUUID() {
        return UUIDs.random().toString();
    }

    /**
     * Insert Data into the database
     *
     * @param table
     * @param values Allowed datatypes: String, int, boolean, LocalDate,
     *               LocalDateTime, List<Object> \n functions like UUID, NOW
     * @return
     */
    public boolean Insert(String table, Map<String, Object> values) {

        Map<String, Object> list = new HashMap<>();

        //set special parameters for some attributes
        for (Map.Entry<String, Object> e : values.entrySet()) {
            String key = e.getKey();
            Object value = e.getValue();

            if (value instanceof Boolean) {
                value = value.toString();
            } else if (value instanceof LocalDateTime) {
                value = value.toString();
            }

            list.put(key, value);
            list.put("dataset_state", 1);
        }

        //prepare the sql statement
        String statement = "INSERT INTO " + Util.keyspace + "." + table + " JSON '" + new Gson().toJson(list).replace("'", "\"") + "'";

        l.info("Statement: " + statement);

        //execute the statement
        try {
            return session.execute(statement).wasApplied();
        } catch (Exception ex) {
            l.error(ex.getMessage(), ex.getCause());
            return false;
        }
    }

    /**
     * Insert Data into the database
     *
     * @param table
     * @param values Allowed datatypes: String, int, boolean, LocalDate,
     *               LocalDateTime, List<Object> \n functions like UUID, NOW
     * @return
     */
    public void InsertAsync(String table, Map<String, Object> values) {

        Map<String, Object> list = new HashMap<>();

        //set special parameters for some attributes
        for (Map.Entry<String, Object> e : values.entrySet()) {
            String key = e.getKey();
            Object value = e.getValue();

            if (value instanceof Boolean) {
                value = value.toString();
            } else if (value instanceof LocalDateTime) {
                value = value.toString();
            }

            list.put(key, value);
            list.put("dataset_state", 1);

        }

        //prepare the sql statement
        String statement = "INSERT INTO " + Util.keyspace + "." + table + " JSON '" + new Gson().toJson(list).replace("'", "\"") + "'";

        l.info("Statement: " + statement);

        //execute
        try {
            session.executeAsync(statement);
        } catch (Exception ex) {
            l.error(ex.getMessage(), ex.getCause());
        }
    }

    /**
     * Check if the UUID is formatted as String
     *
     * @param uuid String that could contain an uuid
     * @return
     */
    private boolean UUIDwrongFormated(String uuid) {
        return Pattern.compile("'[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'").matcher(uuid).find();
    }

    public boolean isUUID(String uuid) {
        return uuid != null && Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}").matcher(uuid).find();
    }

    /**
     * Update values in tables
     *
     * @param table
     * @param values Allowed datatypes: String, int, boolean, LocalDate,
     *               Map<String, Object> \n functions like UUID, NOW
     * @param where
     * @return
     */
    public boolean Update(String table, Map<String, Object> values, String where) {

        //check if a uuid is formatted like a string
        if (UUIDwrongFormated(where)) {
            try {
                throw new Exception("The UUID is not allowed to be formatted like a string. Remove the singlequotes around it.");
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
            }
            return false;
        }

        //check if the datastate is not deleted
        ResultSet res2 = Select(table, new String[]{"dataset_state"}, where, 0, null, null);
        if (res2 == null || res2.one().getInt("dataset_state") < 0) {
            return true;
        }

        //filter out the deleted datasets
        where = where == null ? "" : (" WHERE " + where);

        //generate sql statement
        String assignments = values.keySet().stream().map(x -> x + " = ?").collect(Collectors.joining(", "));
        String statement = "UPDATE " + Util.keyspace + "." + table + " SET " + assignments + where;

        BoundStatement pst = null;

        //prepare statement
        try {
            pst = session.prepare(statement).bind();
        } catch (Exception ex) {
            l.error("Update Statement: " + statement + "   " + ex.getMessage(), ex.getCause());
            return false;
        }

        //build datatype specific json string
        for (Map.Entry<String, Object> e : values.entrySet()) {
            String key = e.getKey();
            Object value = e.getValue();

            String className = value == null ? "null" : value.getClass().getName();
            className = value == null ? "null" : className.substring(className.lastIndexOf('.') + 1);

            switch (className) {
                case "Integer":
                    pst.setInt(key, (int) value);
                    statement = statement.replaceFirst("\\?", value.toString());
                    break;
                case "String":

                    if (isUUID((String) value)) {
                        pst.setUUID(key, UUID.fromString((String) value));
                        statement = statement.replaceFirst("\\?", value.toString());
                    } else {
                        pst.setString(key, (String) value);
                        statement = statement.replaceFirst("\\?", "\"" + value.toString() + "\"");
                    }
                    break;
                case "LocalDateTime":
                    LocalDateTime ldt1 = (LocalDateTime) value;
                    Instant instant1 = ldt1.atZone(ZoneId.systemDefault()).toInstant();
                    Date res1 = Date.from(instant1);
                    pst.setTimestamp(key, (Date) res1);
                    statement = statement.replaceFirst("\\?", "\"" + value.toString() + "\"");
                    break;
                case "LocalDate":
                    LocalDateTime ldt = (LocalDateTime) value;
                    Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
                    Date res = Date.from(instant);
                    pst.setTimestamp(key, (Date) res);
                    statement = statement.replaceFirst("\\?", "\"" + value.toString() + "\"");
                    break;
                case "Boolean":
                    pst.setBool(key, (boolean) value);
                    statement = statement.replaceFirst("\\?", value.toString());
                    break;
                case "Map":
                    pst.setMap(key, (Map<String, Object>) values);
                    statement = statement.replaceFirst("\\?", value.toString());
                    break;
                case "null":
                    pst.setToNull(key);
                    statement = statement.replaceFirst("\\?", "null");
                    break;
                default: {
                    try {
                        throw new Exception("Not allowed datatype(" + className + ") found in an update Statement");
                    } catch (Exception ex) {
                        l.error(ex.getMessage(), ex.getCause());
                    }
                }
                break;
            }
        }

        l.info("Statement: " + statement);

        //execute
        return session.execute(pst).wasApplied();
    }

    /**
     * Delete some database entrys
     *
     * @param table
     * @param where
     * @return
     */
    public boolean Delete(String table, String where) {
        Map<String, Object> clause = new HashMap<>();
        clause.put("dataset_state", 0);

        //make an update of the statement, that it disappears for select und update
        return Update(table, clause, where);
    }

    /**
     * Update values in tables
     *
     * @param table
     * @param values Allowed datatypes: String, int, boolean, LocalDate,
     *               Map<String, Object> \n functions like UUID, NOW
     * @param where
     * @return
     */
    public void UpdateAsync(String table, Map<String, Object> values, String where) {

        //check if a uuid is formatted like a string
        if (UUIDwrongFormated(where)) {
            try {
                throw new Exception("The UUID is not allowed to be formatted like a string. Remove the singlequotes around it.");
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
            }
        } else {
            //check if the dataset is not deleted
            ResultSet res2 = Select(table, new String[]{"dataset_state"}, where, 0, null, null);
            if (res2 != null && res2.one().getInt("dataset_state") > 0) {


                //filter out the deleted datasets
                where = where == null ? "" : (" WHERE " + where);

                //prepare the statement
                String assignments = values.keySet().stream().map(x -> x + " = ?").collect(Collectors.joining(", "));
                String statement = "UPDATE " + Util.keyspace + "." + table + " SET " + assignments + where;

                BoundStatement pst = session.prepare(statement).bind();

                for (Map.Entry<String, Object> e : values.entrySet()) {
                    String key = e.getKey();
                    Object value = e.getValue();


                    String className = value == null ? "null" : value.getClass().getName();
                    className = value == null ? "null" : className.substring(className.lastIndexOf('.') + 1);

                    //handle the datatypes for the JSON Insert
                    switch (className) {
                        case "Integer":
                            pst.setInt(key, (int) value);
                            statement = statement.replaceFirst("\\?", value.toString());
                            break;
                        case "String":

                            if (isUUID((String) value)) {
                                pst.setUUID(key, UUID.fromString((String) value));
                                statement = statement.replaceFirst("\\?", value.toString());
                            } else {
                                pst.setString(key, (String) value);
                                statement = statement.replaceFirst("\\?", "\"" + value.toString() + "\"");
                            }
                            break;
                        case "LocalDateTime":
                            LocalDateTime ldt1 = (LocalDateTime) value;
                            Instant instant1 = ldt1.atZone(ZoneId.systemDefault()).toInstant();
                            Date res1 = Date.from(instant1);
                            pst.setTimestamp(key, (Date) res1);
                            statement = statement.replaceFirst("\\?", "\"" + value.toString() + "\"");
                            break;
                        case "LocalDate":
                            LocalDateTime ldt = (LocalDateTime) value;
                            Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
                            Date res = Date.from(instant);
                            pst.setTimestamp(key, (Date) res);
                            statement = statement.replaceFirst("\\?", "\"" + value.toString() + "\"");
                            break;
                        case "Boolean":
                            pst.setBool(key, (boolean) value);
                            statement = statement.replaceFirst("\\?", value.toString());
                            break;
                        case "Map":
                            pst.setMap(key, (Map<String, Object>) values);
                            statement = statement.replaceFirst("\\?", value.toString());
                            break;
                        case "null":
                            pst.setToNull(key);
                            statement = statement.replaceFirst("\\?", "null");
                            break;
                        default: {
                            try {
                                throw new Exception("Not allowed datatype(" + className + ") found in an update Statement");
                            } catch (Exception ex) {
                                l.error(ex.getMessage(), ex.getCause());
                            }
                        }
                        break;
                    }
                }

                l.info("Statement: " + statement);

                //execute
                session.executeAsync(pst);
            }
        }
    }

    /**
     * Normal select statement with the difference, that the renaming of the columns gets taken there
     *
     * @param table             tablename
     * @param columns           also allowed are the functions MIN, MAX, AVG, SUM and
     *                          COUNT, TODATE
     * @param where             the following operators are allowed: =, IN, =>, <=, <, >,
     *                          AND
     * @param limit             to get all write 0 (max is 10000)
     * @param uuid_last_dataset defines from which row on the data should be
     *                          queried. Needs to be the last uuid of the last query
     * @param orderBy
     * @param call_new_names    The order of the elements needs to be the same as it is called in
     * @return
     */
    public List<Map<String, Object>> Select(String table, String[] columns, String where, int limit, String uuid_last_dataset, String orderBy, String[] call_new_names) {

        List<Row> rows = Select(table, columns, where, limit, uuid_last_dataset, orderBy).all();

        List<Map<String, Object>> list = new ArrayList<>();

        if (call_new_names == null) {   //if no new names are needed
            call_new_names=columns;
        }

        for (Row r : rows) {
            Map<String, Object> map = new HashMap<>();

            for (int i = 0; i < call_new_names.length; i++) {
                map.put(call_new_names[i], r.getObject(i));
            }

            list.add(map);
        }

        return list;
    }

    /**
     * Delete some database entrys
     *
     * @param table
     * @param where
     * @return
     */
    public void DeleteAsync(String table, String where) {
        Map<String, Object> clause = new HashMap<>();
        clause.put("dataset_state", 0);

        UpdateAsync(table, clause, where);
    }
}
