package project.Util;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import org.slf4j.LoggerFactory;
import project.BL.Access;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;

public class Util {

    //NEO4J
    //ServerURI
    public static final String NEO4J_URI = "bolt://neo4j:secret@localhost:7687";

    //CASSANDRA
    //the node of the cassandra server
    public static String clusternode = "127.0.0.1";
    //Node port
    public static int port = 9042;
    //Node user
    //if null, no login credentials are needed
    public static String user = null;
    //public static String user = "r_user";
    //Node password
    public static String password = "BL/upload";
    //Use SSL for the connection
    public static boolean useSSL = false;
    //Name of the keyspace
    public static String keyspace = "testkeyspace";
    //Drops all tables of there are more
    //the replication strategy for the cassandra server
    public static String replication_strategy = "SimpleStrategy";
    //the replication factor for the cassandra server
    public static int replication_factor = 1;
    //if there are more tables in the database then in the DDL
    public static boolean drop_tables_if_too_many = true;

    //Encryption
    //method
    public static String encryption_method = "AES";
    //allow the dynamical changing of the encryption method from other parts of the programm 
    public static boolean allow_encryption_method_runntime_changes = true;
    //path and name of the AES key file
    public static String filepath_AES = "AES_Key.key";
    //path and name of the RSA public key file
    public static String filepath_RSA_public = "RSA_Public_Key.key";
    //path and name of the RSA private key file
    public static String filepath_RSA_private = "RSA_Private_Key.key";

    //Others
    //Default admin user
    public static String adminemail = "adm8392@localhost.local";
    //Default admin password
    public static String adminpassword = "PWneedsTObeCHANGED";
    //Time in minutes between each run of the Cronjob
    public static int pauseBetweenRunningSessionLogoutCron = 5;
    //time in minutes until the user get automatically logged out
    public static int AbsentSessionTime = 60;

    private static String lastError;

    public static boolean checkExternalComponents() {
        if (!PortAvailable(8080)) {
            lastError = "+++ERROR: Another Application uses Port 8080";
        } else if (PortAvailable(9042)) {
            lastError = "+++ERROR: Cassandra is not running";
            System.out.println(lastError);
            System.out.println("It might help to start the service again.");
            System.out.println("Under Windows it is under the Services the DataStax_Cassandra_Community_Server Service");
            System.out.println("If this doesn't work, delete the folder");
            System.out.println("C:\\Program Files\\DataStax Community\\data\\commitlog");
            System.out.println("and start the service again");
        } else if (PortAvailable(7687)) {
            lastError = "+++ERROR: Neo4J is not running";
        } else {
            lastError = "";
            return true;
        }
        return false;
    }

    public static void initEverything() {
        Access.getInstance();
    }


    public static String getLastError() {
        String error = lastError;
        lastError = "";

        return error;
    }

    private static boolean PortAvailable(int port) {
        Socket s = null;
        try {
            s = new Socket("localhost", port);
            return false;
        } catch (IOException e) {
            return true;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (IOException e) {
                    throw new RuntimeException("You should handle this error.", e);
                }
            }
        }
    }

    public static LinkedList<String> getLocalHostLANAddresses() {

        LinkedList<String> addresses = new LinkedList<>();
        try {
            InetAddress localhost = InetAddress.getLocalHost();

            String ip = localhost.toString();
            addresses.add(ip.substring(ip.indexOf("/") + 1));

            // If the host has multiple ip addresses
            InetAddress[] allIPs = InetAddress.getAllByName(localhost.getCanonicalHostName());

            if (allIPs != null && allIPs.length > 1) {
                addresses.clear();

                for (int i = 0; i < allIPs.length; i++) {
                    ip = allIPs[i].toString();
                    addresses.add(ip.substring(ip.indexOf("/") + 1));

                }
            }
        } catch (UnknownHostException e) {
            LoggerFactory.getLogger(Util.class.getClass().getName()).error("Could not get host names");
        }

        return addresses;
    }
}
