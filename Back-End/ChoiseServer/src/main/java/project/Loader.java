package project;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import project.Util.Util;

@SpringBootApplication
public class Loader {

    public static void main(String[] args) {
        if (Util.checkExternalComponents()) {
            SpringApplication.run(project.Loader.class, args);
            Util.initEverything();
        } else {
            System.out.println(Util.getLastError());
            System.exit(0);
        }
    }

}

@Component
class MyListener implements ApplicationListener<ContextRefreshedEvent> {

    public void onApplicationEvent(ContextRefreshedEvent event) {
        LoggerFactory.getLogger(this.getClass().getName()).info("Host is available over: " + Util.getLocalHostLANAddresses());
        LoggerFactory.getLogger(this.getClass().getName()).info("Copyright Manuel Weixler");
    }


}