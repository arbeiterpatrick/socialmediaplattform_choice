
package project.GraphQl;


import org.springframework.beans.factory.annotation.Autowired;
import project.GraphQl.Fetcher.*;
import project.GraphQl.Fields.*;


/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
public class FetcherFieldImports {

    @Autowired
    public MessageDataFetcher messageDataFetcher;
    @Autowired
    public UserDataFetcher userDataFetcher;
    @Autowired
    public FileDataFetcher fileDataFetcher;
    @Autowired
    public ThemeDataFetcher themeDataFetcher;
    @Autowired
    public FriendsDataFetcher friendDataFetcher;
    @Autowired
    public PostDataFetcher postDataFetcher;
    @Autowired
    public ModieDataFetcher modieDataFetcher;
    @Autowired
    public ChatDataFetcher chatDataFetcher;
    @Autowired
    public LikeDataFetcher likeDataFetcher;
    @Autowired
    public CommentDataFetcher commentDataFetcher;
    @Autowired
    public ChallengeDataFetcher challengeDataFetcher;
    @Autowired
    public CompletedChallengesDataFetcher completedChallengeDataFetcher;
    @Autowired
    public LoginDataFetcher loginDataFetcher;

    @Autowired
    public MessageFields messageFields;
    @Autowired
    public ChallengeFields challengeFields;
    @Autowired
    public CommentFields commentFields;
    @Autowired
    public ChatFields chatFields;
    @Autowired
    public ThemeFields themeFields;
    @Autowired
    public FileFields fileFields;
    @Autowired
    public UserFields userFields;
    @Autowired
    public PostFields postFields;
    @Autowired
    public ModieFields modieFields;
    @Autowired
    public LikeFields likeFields;
    @Autowired
    public LoginFields loginFields;
    @Autowired
    public CompletedChallengesFields completedChallengeFields;

}
