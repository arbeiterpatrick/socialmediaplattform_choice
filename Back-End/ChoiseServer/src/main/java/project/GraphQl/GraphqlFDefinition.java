package project.GraphQl;

import com.merapar.graphql.base.TypedValueMap;
import graphql.schema.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.merapar.graphql.base.GraphQlFieldsHelper.*;
import static graphql.schema.GraphQLArgument.newArgument;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLInputObjectField.newInputObjectField;
import static graphql.schema.GraphQLInputObjectType.newInputObject;
import static graphql.schema.GraphQLObjectType.newObject;

/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
public class GraphqlFDefinition {

    protected final GraphQLObjectType.Builder maintype;
    protected static GraphQLObjectType fieldlist;
    private Logger l = LoggerFactory.getLogger(this.getClass().getName());

    public GraphqlFDefinition(String name, String description) {
        //create main definition object
        maintype = newObject().name(name).description(description);
        l.info("Mainpoint " + name + "");
        fieldlist = null;
    }

    public GraphqlFDefinition add(String name, GraphQLScalarType scalar) {
        return add(name, "-NONE-", scalar);
    }

    public GraphqlFDefinition add(String name, String description, GraphQLScalarType scalar) {
        //name has to be set
        if (name.isEmpty()) {
            try {
                throw new Exception("The name can't be empty");
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
            }
        }
        //add a field
        maintype.field(newFieldDefinition().name(name).description(description).type(scalar).build());
        return this;
    }

    public GraphqlFDefinition addRecoursiveItem(String name, String description, String referenceList) {
        //the name needs to be set
        if (name.isEmpty()) {
            try {
                throw new Exception("The name can't be empty");
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
            }
        }

        //add a field
        maintype.field(newFieldDefinition().name(name).description(description).type(
                new GraphQLList(new GraphQLTypeReference(referenceList))
        ).build());
        return this;
    }

    public GraphqlFDefinition addSubList(String name, String description, GraphQLObjectType list, Function<HashMap, List> function) {
        try {
            //try to add the sublist
            maintype.field(newFieldDefinition().name(name).description(description).type(new GraphQLList(list))
                    .dataFetcher(environment -> function.apply(environment.getSource()))
                    .build());
        } catch (Exception e) {
            l.error("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            l.error("A error happened at creating the sublist " + name.toUpperCase() + ".");
            l.error(e.getMessage());
            l.error("The " + (list == null ? "type list" : (function == null ? "function" : "nothing")) + " is not allowed to be null!!!");
            l.error("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            try {
                throw new Exception("A error happened at creating the sublist " + name.toUpperCase() + ".");
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
            }
            System.exit(1);
        }
        return this;
    }

    public GraphQLObjectType getFieldListType() {
        if (fieldlist == null) {
            //the GraphQLObjectType Builder can only build the GraphQLObjectType once
            fieldlist = maintype.build();
            l.debug("Build fieldlist");
        }
        return fieldlist;
    }

    /**
     * This is a normal Mutation field
     * <p>
     * It's purpose is to define how a mutation should look.
     */
    public class Field {

        private String name = "";
        private String decription = "";
        private Function<TypedValueMap, Map> function = null;
        private GraphQLInputObjectType.Builder builder = null;
        private int fieldcounter = 0;

        public Field(String name, String description, Function<TypedValueMap, Map> function) {
            this.name = name;
            this.function = function;
            this.decription = description;
            this.builder = newInputObject().name(name + "Input");
        }

        public Field(String name, String description) {
            this.name = name;
            this.decription = description;
            this.builder = newInputObject().name(name + "Input");
        }

        public Field add(String name, GraphQLScalarType scalar) {
            return add(name, "", scalar);
        }

        public Field add(String name, String description, GraphQLScalarType scalar) {
            //add a field
            builder.field(newInputObjectField().name(name).description(description).type(new GraphQLNonNull(scalar)).build());
            fieldcounter++;
            return this;
        }

        public GraphQLFieldDefinition getFieldDefinition() {
            if (fieldcounter == 0) {
                Exception ex = new Exception("It's not allowed to have no fields. " +
                        "Some external programs that fetch the schema depends on it.");
                l.error(ex.getMessage(), new Throwable(ex));
            }
            try {
                l.info("Build FieldDefinition");
                //build the entire api list
                return newFieldDefinition()
                        .name(name).description(decription).type(getFieldListType())
                        .argument(newArgument().name(INPUT).type(new GraphQLNonNull(builder.build())).build())
                        .dataFetcher(environment -> function.apply(getInputMap(environment)))
                        .build();
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
                return null;
            }
        }
    }

    /**
     * This is a special Mutation field
     * <p>
     * It's purpose is to define the possibilities how a field could be filtered
     */
    public class FilterField {

        private String name = "";
        private String decription = "";
        private Function<TypedValueMap, List<Map<String, Object>>> filterfunction = null;
        protected GraphQLInputObjectType.Builder builder = null;
        private int fieldcounter = 0;

        public FilterField(String name, String description, Function<TypedValueMap, List<Map<String, Object>>> function) {
            this.name = name;
            this.decription = description;
            this.filterfunction = function;
            this.builder = newInputObject().name(name + "FilterInput");
        }

        public FilterField add(String name, GraphQLScalarType scalar) {
            builder.field(newInputObjectField().name(name).type(scalar).build());
            fieldcounter++;
            return this;
        }

        public GraphQLFieldDefinition getFieldDefinition() {
            if (fieldcounter == 0) {
                Exception ex = new Exception("It's not allowed to have no filter set. " +
                        "Some external programs that fetch the schema depends on it.");
                l.error(ex.getMessage(), new Throwable(ex));
            }

            try {
                return newFieldDefinition()
                        .name(name).description(decription)
                        .type(new GraphQLList(getFieldListType()))
                        .argument(newArgument().name(FILTER).type(builder.build()).build())
                        .dataFetcher(environment -> filterfunction.apply(getFilterMap(environment)))
                        .build();
            } catch (Exception ex) {
                l.error(ex.getMessage(), ex.getCause());
                return null;
            }
        }
    }
}
