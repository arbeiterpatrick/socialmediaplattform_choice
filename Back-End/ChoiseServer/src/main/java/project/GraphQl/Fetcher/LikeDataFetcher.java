package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LikeDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getLikesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_likes")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getLikes(context.getActUserToken(), args.get("id"), args.get("type"));
    }

    public List<Map<String, Object>> getLikesByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_likes")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getLikes(context.getActUserToken(), args.get("id").toString(), args.get("type").toString());
    }

    public Map<String, Object> addLike(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_like")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addLike(context.getActUserToken(), args.get("chind"), args.get("id"), args.get("type"));
    }


    public Map<String, Object> deleteLike(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_like")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteLike(context.getActUserToken(), args.get("id"));
    }


}
