package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.List;
import java.util.Map;

@Component
public class FriendsDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getFriendsByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_friends")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getFriends(context.getActUserToken(), args.get("persid"));
    }

    public Map<String, Object> addFriend(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_friend")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addFriend(context.getActUserToken(), args.get("persid"), args.get("relationshiptype"));
    }

    public Map<String, Object> deleteFriend(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_friend")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteFriend(context.getActUserToken(), args.get("id"));
    }
}
