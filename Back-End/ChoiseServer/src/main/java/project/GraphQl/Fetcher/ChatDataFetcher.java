package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.List;
import java.util.Map;

@Component
public class ChatDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getChatsByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_chat")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getChat(context.getActUserToken(), args.get("id"), args.get("persid"));
    }

    public Map<String, Object> deleteChat(TypedValueMap args) {

        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_chat")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteChat(context.getActUserToken(), args.get("id"));
    }
}
