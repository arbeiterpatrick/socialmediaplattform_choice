package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class ChallengeDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getChallengesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_challenges_by_filter")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        if (args != null) {    //some filters are set
            return Access.getInstance().getChallenges(args.get("id"), args.get("name"));
        }

        return Access.getInstance().getChallenges(null, null);
    }

    public List<Map<String, Object>> getChallengesByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_challenges_by_filter")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        if (args != null) {    //some filters are set
            return Access.getInstance().getChallenges(args.get("id").toString(), args.get("name").toString());
        }

        return Access.getInstance().getChallenges(null, null);
    }

    public Map<String, Object> addChallenge(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_challenge")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        int points = -1;
        int level = -1;

        try {
            points = args.get("points") != null ? Integer.parseInt(args.get("points").toString()) : -1;
        } catch (Exception ex) {
            Access.getInstance().addUserError(context.getActUserToken(), "At parsing points happened the following error: " + ex.getMessage(), args.get("points"));
        }

        try {
            level = args.get("level") != null ? Integer.parseInt(args.get("level").toString()) : -1;
        } catch (Exception ex) {
            Access.getInstance().addUserError(context.getActUserToken(), "At parsing level happened the following error: " + ex.getMessage(), args.get("level"));
        }


        return Access.getInstance().addChallenge(
                args.get("name").toString(),
                args.get("teaser").toString(),
                points,
                level,
                context.getActUserToken(),
                args.get("fileid").toString(),
                args.get("postid").toString()
        );
    }

    public Map<String, Object> updateChallenge(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "update_challenge")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        int points = -1;
        int level = -1;

        try {
            points = args.get("points") != null ? Integer.parseInt(args.get("points").toString()) : -1;
        } catch (Exception ex) {
            Access.getInstance().addUserError(context.getActUserToken(), "At parsing points happened the following error: " + ex.getMessage(), args.get("points"));
        }

        try {
            level = args.get("level") != null ? Integer.parseInt(args.get("level").toString()) : -1;
        } catch (Exception ex) {
            Access.getInstance().addUserError(context.getActUserToken(), "At parsing level happened the following error: " + ex.getMessage(), args.get("level"));
        }


        return Access.getInstance().editChallenge(
                args.get("name").toString(),
                args.get("teaser").toString(),
                points,
                level,
                context.getActUserToken(),
                args.get("fileid").toString(),
                args.get("postid").toString(),
                args.get("id").toString()
        );
    }

    public Map<String, Object> deleteChallenge(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_challenge")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        return Access.getInstance().deleteChallenge(context.getActUserToken(), args.get("id").toString());

    }
}
