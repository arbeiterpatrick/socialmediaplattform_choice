package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class LoginDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getLoginsByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_login_info")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        List<Map<String, Object>> list = new ArrayList<>();
        list.add(0, Access.getInstance().getUserInfo(context.getActUserToken()));

        return list;
    }

    public Map<String, Object> Login(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "login")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        Map<String, Object> login = new HashMap<>();

        String token = Access.getInstance().loginUser(args.get("email"), args.get("password"), context.getActUserToken());

        login.put("token", token);
        login.put("operation_successful", token != null);
        login.put("email", args.get("email"));

        return login;
    }

    public Map<String, Object> Logout(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "logout")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        Map<String, Object> login = new HashMap<>();
        login.put("email", args.get("email"));
        login.put("token", null);
        login.put("operation_successful", Access.getInstance().logoutUser(args.get("email"), args.get("token")));

        return login;
    }

    public Map Edit(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "edit_login_credentials")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().editLoginCredentials(context.getActUserToken(), args.get("email"), args.get("persid"), args.get("password"));
    }
}
