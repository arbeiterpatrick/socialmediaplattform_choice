package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.List;
import java.util.Map;

@Component
public class CompletedChallengesDataFetcher {
    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getCompletedChallengesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_completed_challenges")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getCompletedChallenge(context.getActUserToken(), args.get("id"), args.get("persid"), args.get("challengeid"));
    }

    public Map<String, Object> addCompletedChallenge(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_completed_challenge")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addCompletedChallenge(context.getActUserToken(), args.get("challengeid"), args.get("proofpostid"));
    }

    public Map<String, Object> deleteCompletedChallenge(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_completed_challenge")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteCompletedChallenge(context.getActUserToken(), args.get("id"));
    }
}
