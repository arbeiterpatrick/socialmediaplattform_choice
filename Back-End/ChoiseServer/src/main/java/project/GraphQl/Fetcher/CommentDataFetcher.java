package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.List;
import java.util.Map;

@Component
public class CommentDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getCommentsByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_comments")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getComments(context.getActUserToken(), args.get("postid"), args.get("persid"), args.get("commentid"));
    }

    public Map<String, Object> addComment(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_comment")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addComment(context.getActUserToken(), args.get("message"), args.get("postid"));
    }

    public Map<String, Object> updateComment(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "update_comment")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().editComment(context.getActUserToken(), args.get("commentid"), args.get("message"));
    }

    public Map<String, Object> deleteComment(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_comments")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteComment(context.getActUserToken(), args.get("commentid"));
    }

}
