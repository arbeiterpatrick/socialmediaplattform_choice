package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MessageDataFetcher {


    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getMessagesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_messages")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getMessages(context.getActUserToken(), args.get("id"), args.get("chatid"));
    }

    public List<Map<String, Object>> getMessagesByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_messages")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getMessages(context.getActUserToken(), args.get("id").toString(), args.get("chatid").toString());
    }

    public Map<String, Object> addMessage(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_messages")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addMessage(context.getActUserToken(), args.get("message"), args.get("persid"), args.get("fileid"));
    }

    public Map<String, Object> updateMessage(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "update_messages")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().editMessage(context.getActUserToken(), args.get("message"), args.get("id"));
    }

    public Map<String, Object> deleteMessage(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_messages")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteMessage(context.getActUserToken(), args.get("id"));
    }


}
