package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
//import _test_graphql_working.Filefff;
//import lombok.val;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.*;


@Component
public class FileDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getProfilePicture(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_file")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getUserProfilepicture(context.getActUserToken(), args.get("firstname").toString(), args.get("surname").toString());


    }

    public List<Map<String, Object>> getBackgroundImage(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_file")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getUserBackgroundimage(context.getActUserToken(), args.get("firstname").toString(), args.get("surname").toString());

    }

    public List<Map<String, Object>> getFilesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_file")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getFile(context.getActUserToken(), args.get("id"));
    }

    public List<Map<String, Object>> getFilesByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_file")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getFile(context.getActUserToken(), args.get("id").toString());
    }

    public Map<String, Object> addFile(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_file")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addFile(context.getActUserToken(), args.get("name"), args.get("base64contend"));
    }

    public Map<String, Object> deleteFile(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_file")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteFile(context.getActUserToken(), args.get("id"));
    }
}
