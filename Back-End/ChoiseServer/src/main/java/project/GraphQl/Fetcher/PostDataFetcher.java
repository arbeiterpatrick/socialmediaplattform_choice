package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import project.BL.Access;

import project.GraphQl.GraphQLLogin;
//import project.GraphQl.Logs.WLevel;
import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PostDataFetcher {


    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getPostsByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_posts_by_filter")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getPosts(context.getActUserToken(), args.get("postid") == null ? null : args.get("postid").toString(), args.get("persid") == null ? null : args.get("persid").toString());
    }

    public List<Map<String, Object>> getPostsByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_posts_by_filter")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getPosts(context.getActUserToken(), args.get("postid") == null ? null : args.get("postid").toString(), args.get("persid") == null ? null : args.get("persid").toString());
    }

    public Map<String, Object> addPost(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_posts")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().addPost(context.getActUserToken(), args.get("message").toString(), args.get("viewmode").toString(), args.get("fileid").toString());
    }

    public Map<String, Object> updatePost(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "edit_posts")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().editPost(context.getActUserToken(), args.get("id").toString(), args.get("message").toString(), args.get("viewmode").toString(), args.get("fileid").toString());

    }

    public Map<String, Object> deletePost(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "delete_posts")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deletePost(context.getActUserToken(), args.get("id").toString());
    }
}
