package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ModieDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getModiesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_modie")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getModie(context.getActUserToken());
    }

    public List<Map<String, Object>> getModiesByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_modie")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getModie(context.getActUserToken());

    }

    public Map setModie(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "set_modie")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().setModie(context.getActUserToken(), args.get("name"));
    }
}
