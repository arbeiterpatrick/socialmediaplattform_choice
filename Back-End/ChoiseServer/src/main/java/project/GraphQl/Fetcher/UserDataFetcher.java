package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public UserDataFetcher() {
        Access.getInstance().l().info("The methods with the TypedValueMap attribute are the head" +
                " Elements, like when only the users are requested(user) and the HashMap is when the user is" +
                "accessed through the post for example(post->user)");
    }


    public List<Map<String, Object>> getUsersByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_users_by_filter")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getUsers(args.get("id"), context.getActUserToken(), !(args.get("current") == null), args.get("vorname"), args.get("nachname"));
    }

    public List<Map<String, Object>> getUsersByFilter(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_users_by_filter")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getUsers(args.get("persid").toString(), context.getActUserToken(), !(args.get("current") == null), args.get("vorname").toString(), args.get("nachname").toString());
    }

    public List<Map<String, Object>> getUsersFromFriend(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_users_friends")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        return Access.getInstance().getUsersFriends(args.get("persid").toString(), context.getActUserToken());
    }

    public List<Map<String, Object>> getUserCompletedChallenge(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_users_completed_challenge")) {
            throw new GraphQLException("The user is not authorized to do that");
        }
        return Access.getInstance().getUserscompletedChallenge(args.get("id").toString(), context.getActUserToken());
    }

    public Map<String, Object> addUser(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "add_user")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        int size = args.get("fontsize");

        if (size < 1) {
            Access.getInstance().addUserError(Access.getInstance().getPersIDFromToken(context.getActUserToken()), "The given fontsize is not valid", args.get("fontsize"));
        }

        return Access.getInstance().addUser(
                args.get("email"),
                args.get("password"),
                args.get("firstname"),
                args.get("surname"),
                args.get("aboutme"),
                args.get("residence"),
                args.get("birthday"),
                args.get("emplyment"),
                args.get("fontface"),
                args.get("backgroundcolor"),
                size
        );
    }

    public Map<String, Object> updateUser(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "edit_user")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        int size = 0;
        try {
            size = Integer.parseInt(args.get("fontsize"));
            if (size < 1) {
                throw new Exception();
            }
        } catch (Exception ex) {
            Access.getInstance().addUserError(Access.getInstance().getPersIDFromToken(context.getActUserToken()), "The given fontsize is not valid", args.get("fontsize"));
        }

        return Access.getInstance().editUser(
                args.get("persid"),
                context.getActUserToken(),
                args.get("firstname"),
                args.get("surname"),
                args.get("aboutme"),
                args.get("residence"),
                args.get("birthday"),
                args.get("emplyment"),
                args.get("fontface"),
                args.get("backgroundcolor"),
                size
        );
    }

    public Map<String, Object> deleteUser(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "edit_user")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().deleteUser(args.get("persid"), args.get("password"), args.get("email"), context.getActUserToken());
    }
}
