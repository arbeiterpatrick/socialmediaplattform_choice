package project.GraphQl.Fetcher;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.base.TypedValueMap;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.BL.Access;
import project.GraphQl.GraphQLLogin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ThemeDataFetcher {

    @Autowired
    private GraphQLLogin context;

    public List<Map<String, Object>> getTheme(HashMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_theme")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getTheme(context.getActUserToken());
    }


    public List<Map<String, Object>> getThemesByFilter(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "get_theme")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().getTheme(context.getActUserToken());
    }


    public Map setTheme(TypedValueMap args) {
        if (!Access.getInstance().accessAllowed(context.getActUserToken(), "set_theme")) {
            throw new GraphQLException("The user is not authorized to do that");
        }

        return Access.getInstance().setTheme(
                context.getActUserToken(),
                args.get("name"),
                args.get("fontsize"),
                args.get("fontface"),
                args.get("backgroundcolor"),
                args.get("fileid")
        );

    }
}
