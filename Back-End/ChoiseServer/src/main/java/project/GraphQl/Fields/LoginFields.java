package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLString;

@Component
public class LoginFields extends FetcherFieldImports implements GraphQlFields {
//that class error can be ignored because the getters are implemented through lombok

    @Getter
    private GraphQLObjectType loginType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("logins", "A login")
                .add("email", GraphQLString)
                .add("token", GraphQLString)
                .add("persid", GraphQLString)
                .add("operation_successful", GraphQLString);

        loginType = definition.getFieldListType();

        GraphQLFieldDefinition loginsField = definition.new FilterField("logins", "Provide an overview of all logins", loginDataFetcher::getLoginsByFilter).
                add("token", GraphQLString)
                .getFieldDefinition();

        GraphQLFieldDefinition addLoginField = definition.new Field("loginUser", "New login", loginDataFetcher::Login).
                add("email", GraphQLString)
                .add("password", GraphQLString)
                .getFieldDefinition();

        GraphQLFieldDefinition editLoginField = definition.new Field("editLoginCredentials", "New login", loginDataFetcher::Edit).
                add("email", GraphQLString)
                .add("persid", GraphQLString)
                .add("password", GraphQLString).
                        getFieldDefinition();

        GraphQLFieldDefinition deleteLoginField = definition.new Field("logoutUser", "Logout", loginDataFetcher::Logout).
                add("email", GraphQLString).
                add("token", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(loginsField);
        mutationFields = Arrays.asList(addLoginField, editLoginField, deleteLoginField);
    }

}
