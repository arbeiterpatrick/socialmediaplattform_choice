package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.GraphQl.Fetcher.FileDataFetcher;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class FileFields implements GraphQlFields {

    @Autowired
    private FileDataFetcher fileDataFetcher;

    @Getter
    private GraphQLObjectType fileType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        createFileStructure();

    }

    private void createFileStructure() {

        GraphqlFDefinition definition = new GraphqlFDefinition("files", "A file")
                .add("id", "-NONE-", GraphQLString)
                .add("size", "-NONE-", GraphQLInt)
                .add("name", "-NONE-", GraphQLString)
                .add("type", "-NONE-", GraphQLString)
                .add("base64contend", "-NONE-", GraphQLString)
                .add("createdtimestamp", "-NONE-", GraphQLString);

        fileType = definition.getFieldListType();

        GraphQLFieldDefinition filesField = definition.new FilterField("files", "Provide an overview of all files", fileDataFetcher::getFilesByFilter).
                add("id", GraphQLString).
                getFieldDefinition();
        GraphQLFieldDefinition addFileField = definition.new Field("addFile", "Add new file", fileDataFetcher::addFile).
                add("name", GraphQLString).
                add("base64contend", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition deleteFileField = definition.new Field("deleteFile", "Delete existing file", fileDataFetcher::deleteFile).
                add("id", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(filesField);
        mutationFields = Arrays.asList(addFileField, deleteFileField);
    }

}
