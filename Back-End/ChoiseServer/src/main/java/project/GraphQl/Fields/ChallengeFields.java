package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class ChallengeFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType challengeType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("challenges", "A challenge")
                .add("id", GraphQLString)
                .add("level", GraphQLInt)
                .add("points", GraphQLInt)
                .add("popularity", GraphQLInt)
                .add("status", GraphQLInt)
                .add("teaser", GraphQLString)
                .add("name", GraphQLString)
                .add("createdtimestamp", GraphQLString)
                .addSubList("createdByUser", "-NONE-", userFields.getUserType(), userDataFetcher::getUsersByFilter)
                .addSubList("file", "-NONE-", fileFields.getFileType(), fileDataFetcher::getFilesByFilter)
                .addSubList("post", "-NONE-", postFields.getPostType(), postDataFetcher::getPostsByFilter)
                .addSubList("completedByUsers", "-NONE-", userFields.getUserType(), userDataFetcher::getUserCompletedChallenge);

        challengeType = definition.getFieldListType();

        GraphQLFieldDefinition challengesField = definition.new FilterField("challenges", "Provide an overview of all challenges", challengeDataFetcher::getChallengesByFilter).
                add("id", GraphQLString)
                .getFieldDefinition();

        GraphQLFieldDefinition addChallengeField = definition.new Field("addChallenge", "Add new challenge", challengeDataFetcher::addChallenge).
                add("name", GraphQLString)
                .add("teaser", GraphQLString)
                .add("level", GraphQLInt)
                .add("points", GraphQLInt)
                .add("fileid", GraphQLString)
                .add("postid", GraphQLString).
                        getFieldDefinition();

        GraphQLFieldDefinition updateChallengeField = definition.new Field("updateChallenge", "Update existing challenge", challengeDataFetcher::updateChallenge).
                add("name", GraphQLString)
                .add("teaser", GraphQLString)
                .add("level", GraphQLInt)
                .add("points", GraphQLInt)
                .add("id", GraphQLInt)
                .add("fileid", GraphQLString).
                        getFieldDefinition();

        GraphQLFieldDefinition deleteChallengeField = definition.new Field("deleteChallenge", "Delete existing challenge", challengeDataFetcher::deleteChallenge).
                add("id", GraphQLInt).
                getFieldDefinition();

        queryFields = Collections.singletonList(challengesField);
        mutationFields = Arrays.asList(addChallengeField, updateChallengeField, deleteChallengeField);
    }

}
