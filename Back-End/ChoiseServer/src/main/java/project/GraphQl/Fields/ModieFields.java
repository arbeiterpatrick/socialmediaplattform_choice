package project.GraphQl.Fields;

/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class ModieFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType modieType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("modies", "A modie")
                .add("id", GraphQLInt)
                .add("name", GraphQLString);

        modieType = definition.getFieldListType();

        GraphQLFieldDefinition modiesField = definition.new FilterField("modies", "Provide an overview of all modies", modieDataFetcher::getModiesByFilter).
                add("id", GraphQLInt).
                getFieldDefinition();

        GraphQLFieldDefinition setModieField = definition.new Field("setModie", "Set new modie", modieDataFetcher::setModie).
                add("id", GraphQLInt).
                add("name", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(modiesField);
        mutationFields = Arrays.asList(setModieField);
    }

}
