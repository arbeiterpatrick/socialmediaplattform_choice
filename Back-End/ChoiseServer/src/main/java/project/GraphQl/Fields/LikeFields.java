package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class LikeFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType likeType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("likes", "A like")
                .add("id", GraphQLInt)
                .add("chind", GraphQLString)
                .addRecoursiveItem("byUser", "-NONE-", "user")
                .addRecoursiveItem("post", "-NONE-", "posts")
                .addRecoursiveItem("comment", "-NONE-", "comments")
                .addRecoursiveItem("challenge", "-NONE-", "challenges");

        likeType = definition.getFieldListType();

        GraphQLFieldDefinition likesField = definition.new FilterField("likes", "Provide an overview of all likes", likeDataFetcher::getLikesByFilter).
                add("id", GraphQLInt).
                add("type", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition addLikeField = definition.new Field("addLike", "Add new like", likeDataFetcher::addLike).
                add("chind", GraphQLString).
                add("type", GraphQLString).
                add("id", GraphQLString).
                getFieldDefinition();


        GraphQLFieldDefinition deleteLikeField = definition.new Field("deleteLike", "Delete existing like", likeDataFetcher::deleteLike).
                add("id", GraphQLInt).
                getFieldDefinition();

        queryFields = Collections.singletonList(likesField);
        mutationFields = Arrays.asList(addLikeField, deleteLikeField);
    }

}
