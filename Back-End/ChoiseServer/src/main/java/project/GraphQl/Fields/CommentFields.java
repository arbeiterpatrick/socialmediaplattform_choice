package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;
import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class CommentFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType commentType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("comments", "A comment")
                .add("postid", GraphQLString)
                .add("commentid", GraphQLString)
                .add("createdTimestamp", GraphQLString)
                .add("editedTimestamp", GraphQLString)
                .add("message", GraphQLString)
                .addRecoursiveItem("byUser", "-NONE-", "user")
                .addSubList("likes", "-NONE-", likeFields.getLikeType(), likeDataFetcher::getLikesByFilter);

        commentType = definition.getFieldListType();

        GraphQLFieldDefinition commentsField = definition.new FilterField("comments", "Provide an overview of all comments", commentDataFetcher::getCommentsByFilter).
                add("commentid", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition addCommentField = definition.new Field("addComment", "Add new comment", commentDataFetcher::addComment).
                add("postid", GraphQLString).
                add("message", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition updateCommentField = definition.new Field("updateComment", "Update existing comment", commentDataFetcher::updateComment).
                add("commentid", GraphQLString).
                add("message", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition deleteCommentField = definition.new Field("deleteComment", "Delete existing comment", commentDataFetcher::deleteComment).
                add("commentid", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(commentsField);
        mutationFields = Arrays.asList(addCommentField, updateCommentField, deleteCommentField);
    }

}
