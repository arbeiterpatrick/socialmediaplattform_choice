package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class FriendsFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    public GraphQLObjectType friendType;
    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("friends", "A friend")
                .add("relationshiptype", GraphQLString)
                .addSubList("user", "-NONE-", userFields.getUserType(), userDataFetcher::getUsersFromFriend);

        friendType = definition.getFieldListType();

        GraphQLFieldDefinition friendsField = definition.new FilterField("friends", "Provide an overview of all friends", friendDataFetcher::getFriendsByFilter).
                add("persid", GraphQLInt).
                getFieldDefinition();

        GraphQLFieldDefinition addFriendField = definition.new Field("addFriend", "Add new friend", friendDataFetcher::addFriend).
                add("persid", GraphQLInt).
                add("relationshiptype", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition deleteFriendField = definition.new Field("deleteFriend", "Delete existing friend", friendDataFetcher::deleteFriend).
                add("id", GraphQLInt).
                getFieldDefinition();

        queryFields = Collections.singletonList(friendsField);
        mutationFields = Arrays.asList(addFriendField, deleteFriendField);
    }

}
