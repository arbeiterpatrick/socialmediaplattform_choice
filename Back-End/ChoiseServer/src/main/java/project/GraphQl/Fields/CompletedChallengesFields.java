package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLString;

@Component
public class CompletedChallengesFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType completedChallengeType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("completedChallenges", "A completedChallenge")
                .add("id", GraphQLString)
                .addRecoursiveItem("challenge", "-NONE-", "challenges")
                .addRecoursiveItem("completedBy", "-NONE-", "user")
                .addRecoursiveItem("proofPost", "-NONE-", "posts");

        completedChallengeType = definition.getFieldListType();

        GraphQLFieldDefinition completedChallengesField = definition.new FilterField("completedChallenges", "Provide an overview of all completedChallenges", completedChallengeDataFetcher::getCompletedChallengesByFilter).
                add("id", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition addCompletedChallengeField = definition.new Field("addCompletedChallenge", "Add new completedChallenge", completedChallengeDataFetcher::addCompletedChallenge).
                add("challengeid", GraphQLString).
                add("proofpostid", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition deleteCompletedChallengeField = definition.new Field("deleteCompletedChallenge", "Delete existing completedChallenge", completedChallengeDataFetcher::deleteCompletedChallenge).
                add("id", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(completedChallengesField);
        mutationFields = Arrays.asList(addCompletedChallengeField, deleteCompletedChallengeField);
    }

}
