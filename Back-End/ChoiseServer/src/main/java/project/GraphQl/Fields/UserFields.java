package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.*;

@Component
public class UserFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType userType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("user", "A user")
                .add("persid", "-NONE-", GraphQLString)
                .add("permissionlevel", "-NONE-", GraphQLInt)
                .add("challengepoints", "-NONE-", GraphQLInt)
                .add("firstname", "-NONE-", GraphQLString)
                .add("surname", "-NONE-", GraphQLString)
                .add("email", "-NONE-", GraphQLString)
                .add("aboutme", "-NONE-", GraphQLString)
                .add("residence", "-NONE-", GraphQLString)
                .add("birthday", "-NONE-", GraphQLString)
                .add("employment", "-NONE-", GraphQLString)
                .add("fontface", "-NONE-", GraphQLString)
                .add("backgroundcolor", "-NONE-", GraphQLString)
                .add("fontsize", "-NONE-", GraphQLInt)
                .addSubList("profilepicture", "-NONE-", fileFields.getFileType(), fileDataFetcher::getProfilePicture)
                .addSubList("theme", "-NONE-", themeFields.getThemeType(), themeDataFetcher::getTheme)
                .addSubList("posts", "-NONE-", postFields.getPostType(), postDataFetcher::getPostsByFilter)
                .addSubList("modie", "-NONE-", modieFields.getModieType(), modieDataFetcher::getModiesByFilter);

        userType = definition.getFieldListType();

        GraphQLFieldDefinition usersField = definition.new FilterField("users", "Provide an overview of all users", userDataFetcher::getUsersByFilter).
                add("id", GraphQLInt).
                add("current", GraphQLBoolean).
                add("vorname", GraphQLString).
                add("nachname", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition addUserField = definition.new Field("addUser", "Add new user", userDataFetcher::addUser).
                add("firstname", "-NONE-", GraphQLString)
                .add("surname", "-NONE-", GraphQLString)
                .add("email", "-NONE-", GraphQLString)
                .add("aboutme", "-NONE-", GraphQLString)
                .add("residence", "-NONE-", GraphQLString)
                .add("birthday", "-NONE-", GraphQLString)
                .add("emplyment", "-NONE-", GraphQLString)
                .add("fontface", "-NONE-", GraphQLString)
                .add("password", "-NONE-", GraphQLString)
                .add("backgroundcolor", "-NONE-", GraphQLString)
                .add("fontsize", "-NONE-", GraphQLInt)
                .getFieldDefinition();

        GraphQLFieldDefinition updateUserField = definition.new Field("updateUser", "Update existing user", userDataFetcher::updateUser).
                add("firstname", "-NONE-", GraphQLString)
                .add("surname", "-NONE-", GraphQLString)
                .add("email", "-NONE-", GraphQLString)
                .add("aboutme", "-NONE-", GraphQLString)
                .add("residence", "-NONE-", GraphQLString)
                .add("birthday", "-NONE-", GraphQLString)
                .add("emplyment", "-NONE-", GraphQLString)
                .add("fontface", "-NONE-", GraphQLString)
                .add("backgroundcolor", "-NONE-", GraphQLString)
                .add("fontsize", "-NONE-", GraphQLInt)
                .getFieldDefinition();

        GraphQLFieldDefinition deleteUserField = definition.new Field("deleteUser", "Delete existing user", userDataFetcher::deleteUser).
                add("persid", GraphQLString).
                add("password", GraphQLString).
                add("email", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(usersField);
        mutationFields = Arrays.asList(addUserField, updateUserField, deleteUserField);
    }
}
