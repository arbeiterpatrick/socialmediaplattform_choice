package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.GraphQl.Fetcher.FileDataFetcher;
import project.GraphQl.Fetcher.ThemeDataFetcher;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class ThemeFields implements GraphQlFields {

    @Autowired
    private ThemeDataFetcher themeDataFetcher;

    @Autowired
    private FileFields fileFields;

    @Autowired
    private FileDataFetcher fileDataFetcher;

    @Getter
    private GraphQLObjectType themeType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {

        GraphqlFDefinition definition = new GraphqlFDefinition("theme", "A theme")
                .add("fontsize", GraphQLInt)
                .add("name", GraphQLString)
                .add("fontface", GraphQLString)
                .add("backgroundcolor", GraphQLString)
                .addSubList("backgroundimage", "-NONE-", fileFields.getFileType(), fileDataFetcher::getBackgroundImage);

        themeType = definition.getFieldListType();

        GraphQLFieldDefinition themesField = definition.new FilterField("theme", "Provide an overview of all themes", themeDataFetcher::getThemesByFilter).
                add("id", GraphQLInt).
                getFieldDefinition();

        GraphQLFieldDefinition addThemeField = definition.new Field("setTheme", "Add new theme", themeDataFetcher::setTheme).
                add("fontsize", GraphQLInt)
                .add("name", GraphQLString)
                .add("fontface", GraphQLString)
                .add("backgroundcolor", GraphQLString)
                .add("fileid", GraphQLString)
                .getFieldDefinition();

        queryFields = Collections.singletonList(themesField);
        mutationFields = Arrays.asList(addThemeField);
    }

}
