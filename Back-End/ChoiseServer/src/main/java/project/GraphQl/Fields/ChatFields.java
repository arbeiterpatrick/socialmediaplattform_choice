package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;

@Component
public class ChatFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType chatType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("chats", "A chat")
                .add("id", GraphQLString)
                .add("unreadmessages", GraphQLInt)
                .add("received_timestamp", GraphQLString)
                .add("messagepreview", GraphQLString)
                .addSubList("user", "-NONE-", userFields.getUserType(), userDataFetcher::getUsersByFilter)
                .addRecoursiveItem("message", "-NONE-", "messages");

        chatType = definition.getFieldListType();

        GraphQLFieldDefinition chatsField = definition.new FilterField("chats", "Provide an overview of all chats", chatDataFetcher::getChatsByFilter).
                add("id", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition deleteChatField = definition.new Field("deleteChat", "Delete existing chat", chatDataFetcher::deleteChat).
                add("id", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(chatsField);
        mutationFields = Arrays.asList(deleteChatField);
    }

}
