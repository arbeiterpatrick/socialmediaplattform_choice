package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;


@Component
public class PostFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType postType;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {

        GraphqlFDefinition definition = new GraphqlFDefinition("posts", "A post")
                .add("id", GraphQLString)
                .add("createdtimestamp", GraphQLString)
                .add("editedtimestamp", GraphQLString)
                .add("message", GraphQLString)
                .add("viewmode", GraphQLInt)
                .addSubList("file", "-NONE-", fileFields.getFileType(), fileDataFetcher::getFilesByFilter)
                .addSubList("likes", "-NONE-", likeFields.getLikeType(), likeDataFetcher::getLikesByFilter)
                .addRecoursiveItem("comments", "-NONE-", "comments")
                .add("byuser", GraphQLString)
                .add("firstname", GraphQLString)
                .add("surname", GraphQLString);

        postType = definition.getFieldListType();

        GraphQLFieldDefinition postsField = definition.new FilterField("posts", "Provide an overview of all posts", postDataFetcher::getPostsByFilter).
                add("id", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition addPostField = definition.new Field("addPost", "Add new post", postDataFetcher::addPost).
                add("message", GraphQLString)
                .add("fileid", GraphQLString)
                .add("viewmode", GraphQLInt)
                .getFieldDefinition();

        GraphQLFieldDefinition updatePostField = definition.new Field("updatePost", "Update existing post", postDataFetcher::updatePost).
                add("message", GraphQLString)
                .add("fileid", GraphQLString)
                .add("viewmode", GraphQLInt)
                .add("id", GraphQLString)
                .getFieldDefinition();

        GraphQLFieldDefinition deletePostField = definition.new Field("deletePost", "Delete existing post", postDataFetcher::deletePost).
                add("id", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(postsField);
        mutationFields = Arrays.asList(addPostField, updatePostField, deletePostField);
    }

}
