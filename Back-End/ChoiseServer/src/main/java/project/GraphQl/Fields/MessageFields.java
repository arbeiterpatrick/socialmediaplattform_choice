package project.GraphQl.Fields;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.GraphQlFields;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import lombok.Getter;
import org.springframework.stereotype.Component;
import project.GraphQl.FetcherFieldImports;
import project.GraphQl.GraphqlFDefinition;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static graphql.Scalars.GraphQLString;

@Component
public class MessageFields extends FetcherFieldImports implements GraphQlFields {

    @Getter
    private GraphQLObjectType messageTypes;

    @Getter
    private List<GraphQLFieldDefinition> queryFields;

    @Getter
    private List<GraphQLFieldDefinition> mutationFields;

    @PostConstruct
    public void postConstruct() {
        GraphqlFDefinition definition = new GraphqlFDefinition("messages", "A message")
                .add("id", GraphQLString)
                .add("createdtimestamp", GraphQLString)
                .add("editedtimestamp", GraphQLString)
                .add("message", GraphQLString)
                .addSubList("file", "-NONE-", fileFields.getFileType(), fileDataFetcher::getFilesByFilter);

        messageTypes = definition.getFieldListType();

        GraphQLFieldDefinition messagesField = definition.new FilterField("messages", "Provide an overview of all messages", messageDataFetcher::getMessagesByFilter).
                add("id", GraphQLString).
                add("chatid", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition addMessageField = definition.new Field("sendMessage", "Send a new Message", messageDataFetcher::addMessage).
                add("message", GraphQLString).
                add("persid", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition updateMessageField = definition.new Field("updateMessage", "Update existing message", messageDataFetcher::updateMessage).
                add("id", GraphQLString).
                add("name", GraphQLString).
                getFieldDefinition();

        GraphQLFieldDefinition deleteMessageField = definition.new Field("deleteMessage", "Delete existing message", messageDataFetcher::deleteMessage).
                add("id", GraphQLString).
                getFieldDefinition();

        queryFields = Collections.singletonList(messagesField);
        mutationFields = Arrays.asList(addMessageField, updateMessageField, deleteMessageField);
    }

}
