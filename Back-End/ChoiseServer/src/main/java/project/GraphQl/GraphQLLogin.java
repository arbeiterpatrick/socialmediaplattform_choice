package project.GraphQl;
/**
 * @author Manuel Weixler
 * @email manuel@weixler.me
 * @copyright Manuel Weixler
 */

import com.merapar.graphql.executor.GraphQlExecutorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import project.BL.Access;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Component
public class GraphQLLogin extends GraphQlExecutorImpl {

    @Autowired
    private HttpServletRequest request;
    private String actUserToken;
    @Value("${identifier:null}")
    private String identifier;

    public String getActUserToken() {
        return identifier;
    }


    private String getHeadersInfo() {

        if (request == null) {
            return null;
        }

        Map<String, String> map = new HashMap<>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map.toString();
    }


    @Override
    protected void beforeExecuteRequest(String query, String operationName, Map<String, Object> context, Map<String, Object> variables) {

        init();
        Access.getInstance().addAccessAttempt(getHeadersInfo(), query, actUserToken);

        identifier = actUserToken;
        Access.getInstance().l().info("The session token is " + identifier);
        Access.getInstance().l().warn("I found no way to restrict the stacktrace when an error occurs.");
    }


    private void init() {
        actUserToken = null;

        if (request != null) {
            Access.getInstance().setIP(request.getRemoteAddr());

            Enumeration headerNames = request.getHeaderNames();

            while (headerNames.hasMoreElements()) {
                String key = (String) headerNames.nextElement();

                if (key.toLowerCase().equals("authorization")) {
                    actUserToken = request.getHeader(key).replace("Bearer", "").trim();
                }
            }
        }
    }

}

